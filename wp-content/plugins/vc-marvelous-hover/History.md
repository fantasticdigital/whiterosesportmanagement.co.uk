
2.0.6 / 2017-12-17
==================

  * Update: correct the requirement for visual composer version

2.0.5 / 2017-12-13
==================

  * Update plugin version label
  * Fix: php warning const missing
  * Fix: isotope missing
  * Fix: issue with Isotope
  * Fix: conflict with vckit
  * Update: remove required visual composer in package

2.0.4 / 2017-11-24
==================

  * Update: version label
  * Fix: overlay class which we do not use
  * Fix: zoe description
  * Fix: missing declare variable before use
  * Fix: visual composer conflict with theme which already define WPB_VC_VERSION constant
  * Update: cmb2 switch should work for all themelego element
  * Update: image swap - add option to hide shadow

2.0.3 / 2017-10-04
==================

  * Update: add link option to image swap

2.0.2 / 2017-10-04
==================

  * Fix: wrong build link

2.0.1 / 2017-09-28
==================

  * Fix: conflict with endfold theme
  * Fix: marvelous grid cannot add custom effect for each post
  * Fix: conflict with visual composer waypoint library

2.0.0 / 2017-09-25
==================

  * Fix: base line-height
  * Fix: monroe effect line height
  * Fix: effect-caption error in some effect
  * Fix: get googleFonts only work with Visual Composer
  * Fix: build tool
  * Fix: cobble set overlay default 0
  * Update: bring pretty photo to package
  * Fix: caracas effect-description height
  * Fix: Monroe, Wilbert, Praia, Brasila - title on the top
  * Fix: effect cobbles
  * Fix: param vc_link and textfield
  * Update: fix phpcs warning
  * Feature: wordpress version
  * Fix: remove pixel icon out of the package
  * Fix: rename vc_marvelous_hover to marvelous-hover
  * Fix: php codesniffer warning
  * Update: build tool add code sniffer
  * Update: move vc_col to marvelous grid framework
  * Update: bring css animation out of visual composer
  * Update: add method to check visual composer actived
  * Update: move vc_post_param to marvelous_post_param
  * Update: move vc_taxonomies to marvelous_taxonomies
  * Update: move vc_value_from_safe to marvelous_value_from_safe
  * Update: move vc_marvelous_hover to marvelous_hover
  * Update: add design option to marvelous image tilt, marvelous tilt
  * Update: add design option to image swap
  * Update: move vc_shortcode_custom_css_class to marvelous_shortcode_custom_css_class
  * Update: change require to require_once
  * Update: change integrate with visual composer
  * Fix: naming image tilt effect
  * Update: move vc_map_get_attributes to marvelous_map_get_attributes
  * Fix: remove vc_enabled role check when save post
  * Update: bring animate.css to self marvelous hover
  * Update: bring no_image from visual composer to marvelous hover
  * Feature: use image alt instead of heading thin and bold
  * Feature: effect male
  * Feature: effect malabo
  * Feature: effect camberra
  * Feature: effect caracas
  * Update: move get marvelous custom css to base
  * Fix: oslo
  * Update: basic margin and effect caption default z-index
  * Feature: effect oslo
  * Feature: effect brasilia
  * Feature: effect praia
  * Feature: effect wilbert
  * Feature: effect monroe
  * Update: mixin overlay
  * Update: structure of marvelous hover layer content
  * Update: washington remove background overlay
  * Upgrade: build tool
  * Version 2 label
  * Add: image tilt and image swap
  * Remove unuse function
  * Fix: add z index for image

1.3.1 / 2017-06-03
==================

  * Version label
  * Fix: tilt style 6
  * Fix: oscar
  * Fix: zoe
  * Fix: layla
  * Fix: Tilt hover effect 6, 7 and effect-sm
  * Fix: tilt hover add ratio 415/300
  * Improve shortcode hook
  * Remove modernizr
  * Remove tutorial
  * Fix: grid min-height base
  * Rollback post title as a description not a title
  * Fix: add width: 100% for image
  * Fix: lily and julia max-width img
  * Fix: Julia issue
  * Fix: set3 empty content issue

1.3.0 / 2017-05-23
==================

  * Version 1.3
  * Enable images size by default
  * Set 3 for grid
  * Feature: Carter
  * Feature: Jefferson
  * Feature: Adams
  * Feature: Madison
  * Feature: Cobbles
  * Add: Jackson effect
  * Feature: VanBuren
  * Feature: Washington
  * Marvelous tilt hover - new shortcode
  * Grid with lightbox
  * Feature: prettyphoto lightbox
  * Feature: custom image size bring back
  * Fix: issue with custom class
  * Fix: ratio34 image dimention
