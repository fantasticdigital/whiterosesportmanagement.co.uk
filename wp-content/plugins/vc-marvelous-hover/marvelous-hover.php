<?php
/*
Plugin Name: Marvelous Hover for Visual Composer
Plugin URI: http://marveloushover.themelego.com/
Description: An inspirational collection of subtle hover effects for Visual Composer inspired by http://tympanus.net/codrops/2014/06/19/ideas-for-subtle-hover-effects/
Version: 2.0.6
Author: Themelego
Author URI: http://themelego.com
Inspired by: http://tympanus.net/codrops/2014/06/19/ideas-for-subtle-hover-effects/
*/

/*
More information can be found here: http://kb.wpbakery.com/index.php?title=Category:Visual_Composer
*/

/**
 * Copyright (c) 2017 ThemeLego (email : support@themelego.com)
 */

if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly.
}

define( 'MARVELOUS_PLUGIN', __FILE__ );
define( 'MARVELOUS_PLUGIN_DIR', untrailingslashit( dirname( MARVELOUS_PLUGIN ) ) );

define( 'VCKIT_SHORTCODE_CUSTOMIZE_PREFIX', 'vckit_theme_' );
define( 'VCKIT_SHORTCODE_BEFORE_CUSTOMIZE_PREFIX', 'vckit_theme_before_' );
define( 'VCKIT_SHORTCODE_AFTER_CUSTOMIZE_PREFIX', 'vckit_theme_after_' );

// Include additional php files here.
require_once MARVELOUS_PLUGIN_DIR . '/inc/core-functions.php';
require_once MARVELOUS_PLUGIN_DIR . '/inc/themelego-helpers.php';
require_once MARVELOUS_PLUGIN_DIR . '/inc/templates.php';
require_once MARVELOUS_PLUGIN_DIR . '/inc/metabox/metabox-types.php';
require_once MARVELOUS_PLUGIN_DIR . '/inc/metabox/marvelous-grid.php';
require_once MARVELOUS_PLUGIN_DIR . '/inc/settings.php';

require_once MARVELOUS_PLUGIN_DIR . '/inc/shortcodes/base.php';
require_once MARVELOUS_PLUGIN_DIR . '/inc/shortcodes/marvelous.php';
require_once MARVELOUS_PLUGIN_DIR . '/inc/shortcodes/grid.php';
require_once MARVELOUS_PLUGIN_DIR . '/inc/shortcodes/grid/marvelous.php';
require_once MARVELOUS_PLUGIN_DIR . '/inc/shortcodes/tilt.php';
require_once MARVELOUS_PLUGIN_DIR . '/inc/shortcodes/image-swap.php';
require_once MARVELOUS_PLUGIN_DIR . '/inc/shortcodes/image-tilt.php';

/**
 * Main initiation class
 *
 * @since  1.0
 */
final class VCMarvelousHover {

  /**
   * Current version
   *
   * @var  string
   * @since  1.0.0
   */
  const VERSION = '2.0.6';

  /**
   * URL of plugin directory
   *
   * @var string
   * @since  1.0
   */
  protected $url = '';

  /**
   * Path of plugin directory
   *
   * @var string
   * @since  1.0
   */
  protected $path = '';

  /**
   * Plugin basename
   *
   * @var string
   * @since  1.0
   */
  protected $basename = '';

  /**
   * Detailed activation error messages
   *
   * @var array
   * @since  1.0
   */
  protected $activation_errors = array();

  /**
   * Singleton instance of plugin
   *
   * @var VCMarvelousHover
   * @since  1.0
   */
  protected static $single_instance = null;

  /**
   * Default template dir
   *
   * @var String
   * @since  1.0
   */
  protected $template_path;

  /**
   * Custom user templates dir
   *
   * @var String
   * @since  1.0
   */
  protected $custom_user_templates_dir = false;

  /**
   * Flag default_scripts_enqueued
   */
  public static $default_scripts_enqueued = false;

  /**
   * Shortcodes
   */
  protected $shortcodes;

  /**
   * Instance of Marvelous Options
   *
   * @since 0.1
   * @var Marvelous_Options
   */
  protected $options;

  /**
   * TinyMCE tag
   *
   * @since 2.0
   */
  protected $tinymce_tag;

  /**
   * Creates or returns an instance of this class.
   *
   * @since  1.0
   * @return VCMarvelousHover A single instance of this class.
   */
  public static function get_instance() {
    if ( null === self::$single_instance ) {
      self::$single_instance = new self();
    }

    return self::$single_instance;
  }

  /**
   * Sets up our plugin
   *
   * @since  1.0
   */
  protected function __construct() {
    $this->basename = plugin_basename( __FILE__ );
    $this->url      = plugin_dir_url( __FILE__ );
    $this->path     = plugin_dir_path( __FILE__ );

    $this->template_path = $this->path . 'templates/';
    $this->tinymce_tag = 'marvelous_shortcodes';
  }

  /**
   * Attach other plugin classes to the base plugin class.
   *
   * @since  1.0
   * @return void
   */
  public function plugin_classes() {
    $this->options = new Marvelous_Options( $this );
    // Attach other plugin classes to the base plugin class.
    $this->shortcodes['marvelous_grid'] = new Marvelous_Shortcode_Grid_Item( $this );
    $this->shortcodes['marvelous_hover'] = new Marvelous_Shortcode_Item( $this );
    $this->shortcodes['marvelous_tilt'] = new Marvelous_Shortcode_Tilt( $this );
    $this->shortcodes['marvelous_image_tilt'] = new Marvelous_Shortcode_Image_Tilt( $this );
    $this->shortcodes['marvelous_image_swap'] = new Marvelous_Shortcode_Image_Swap( $this );
  } // END OF PLUGIN CLASSES FUNCTION

  /**
   * Add hooks and filters
   *
   * @since  1.0
   * @return void
   */
  public function hooks() {
    // Priority needs to be:
    // < 10 for CPT_Core,
    // < 5 for Taxonomy_Core,
    // 0 Widgets because widgets_init runs at init priority 1.
    add_action( 'init', array( $this, 'init' ), 0 );
  }

  /**
   * Init hooks
   *
   * @since  1.0
   * @return void
   */
  public function init() {
    // bail early if requirements aren't met
    if ( ! $this->check_requirements() ) {
      return;
    }

    add_action( 'admin_enqueue_scripts', array( &$this, 'adminCss' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'enqueueDefaultStyle' ) );
    add_action( 'template_redirect', array( &$this, 'registerDefaultScripts' ) );
    add_action( 'save_post', array( $this, 'onPostSave' ) );
    add_action( 'wp_head', array( $this, 'addPageCustomCss' ), 1000 );
    add_action( 'wp_ajax_marvelous_shortcode', array( $this, 'getMarvelousShortcode' ) );

    // load translated strings for plugin
    load_plugin_textdomain( 'legocreative', false, dirname( $this->basename ) . '/languages/' );

    // initialize plugin classes
    $this->intergrateTinyMCE();
    $this->plugin_classes();
    $this->registerMetabox();
    $this->notices();
  }

  /**
   * Set user directory name.
   *
   * Directory name is the directory name vc should scan for custom shortcodes template.
   *
   * @since    1.0
   * @access   public
   *
   * @param $dir - path to shortcodes templates inside developers theme
   */
  public function setCustomUserShortcodesTemplateDir( $dir ) {
    preg_replace( '/\/$/', '', $dir );
    $this->custom_user_templates_dir = $dir;
  }

  /**
   * Get default directory where shortcodes templates area placed.
   *
   * @since  1.0
   * @access public
   *
   * @return string - path to default shortcodes
   */
  public function getDefaultShortcodesTemplatesDir() {
    return $this->template_path;
  }

  /**
   *
   * Get shortcodes template dir.
   *
   * @since  1.0
   * @access public
   *
   * @param $template
   *
   * @return string
   */
  public function getShortcodesTemplateDir( $template ) {
    return false !== $this->custom_user_templates_dir ? $this->custom_user_templates_dir . '/' . $template : locate_template( 'marvelous_templates/' . $template );
  }

  /**
   * Gets absolute path for file/directory in filesystem.
   *
   * @since  4.2
   * @access public
   *
   * @param $name - name of path dir
   * @param string $file - file name or directory inside path
   *
   * @return string
   */
  public function path( $name, $file = '' ) {
    $path = $this->template_path . $name . ( strlen( $file ) > 0 ? '/' . preg_replace( '/^\//', '', $file ) : '' );

    return apply_filters( 'marvelous_path_filter', $path );
  }

  public function adminCss( $hook ) {
    wp_enqueue_style( 'marvelous-admin-css', plugins_url( 'assets/css/admin-styles.css', __FILE__ ) );
    wp_enqueue_script( 'marvelous-admin-js', plugins_url( 'assets/js/admin-js.js', __FILE__ ) );
  }

  /**
   * save hook
   * @see include/classes/core/class-vc-post-admin.php
   */
  public function onPostSave( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
      return;
    }

    $this->buildShortcodesCustomCss( $post_id );
  }

  /**
   * Build custom css styles for page from shortcodes attributes created by VC editors.
   *
   * Called by save method, which is hooked by edit_post action.
   * Function creates meta data for post with the key '_wpb_shortcodes_custom_css'
   * and value as css string, which will be added to the footer of the page.
   *
   * @see include/classes/core/class-vc-base.php
   * @access public
   *
   * @param $post_id
   */
  public function buildShortcodesCustomCss( $post_id ) {
    $post = get_post( $post_id );
    /**
     * vc_filter: vc_base_build_shortcodes_custom_css
     * @since 4.4
     */

    $css = apply_filters( 'marvelous_build_shortcodes_custom_css', $this->parseShortcodesCustomCss( $post->post_content ) );
    if ( empty( $css ) ) {
      delete_post_meta( $post_id, '_marvelous_shortcodes_custom_css' );
    } else {
      update_post_meta( $post_id, '_marvelous_shortcodes_custom_css', $css );
    }
  }

  /**
   * Add all mapped shortcodes
   *
   * @since  1.0.0
   * @return void
   */
  public function addAllMappedShortcodes() {
    foreach ( $this->shortcodes as $shortcode ) {
      if ( ! shortcode_exists( $shortcode->getShortcode() ) ) {
        add_shortcode( $shortcode->getShortcode(), array( $shortcode, 'renderShortcode' ) );
      }
    }
  }

  public function getAllShortcodes() {
    $shortcodes = array();
    foreach ( $this->shortcodes as $shortcode ) {
      $shortcodes[] = $shortcode->getShortcode();
    }

    return $shortcodes;
  }

  /**
   * Parse shortcodes custom css string.
   *
   * This function is used by self::buildShortcodesCustomCss and creates css string from shortcodes attributes
   * like 'css_editor'.
   *
   * @see    include/classes/core/class-vc-base.php
   *
   * @param $content
   *
   * @return string
   */
  public function parseShortcodesCustomCss( $content ) {
    $css = '';
    if ( ! preg_match( '//', $content ) ) {
      return $css;
    }

    // Map all
    $this->addAllMappedShortcodes();

    preg_match_all( '/' . get_shortcode_regex( $this->getAllShortcodes() ) . '/', $content, $shortcodes );
    foreach ( $shortcodes[2] as $index => $tag ) {
      foreach ( $this->shortcodes as $sc ) {
        if ( $sc->getShortcode() == $tag ) {
          $atts = shortcode_parse_atts( trim( $shortcodes[3][ $index ] ) );
          $css .= $sc->getCustomCSS( $atts );

          break;
        }
      }
    }

    // Shortcode inside shortcode
    // EX: button inside animation block
    foreach ( $shortcodes[5] as $shortcode_content ) {
      $css .= $this->parseShortcodesCustomCss( $shortcode_content );
    }

    return $css;
  }

  /**
   * Hooked class method by wp_head WP action to output post custom css.
   *
   * Method gets post meta value for page by key '_wpb_post_custom_css' and if it is not empty
   * outputs css string wrapped into style tag.
   *
   * @since  1.0.0 VC 4.2
   * @access public
   *
   * @param int $id
   */
  public function addPageCustomCss( $id = null ) {
    if ( ! is_singular() ) {
      return;
    }
    if ( ! $id ) {
      $id = get_the_ID();
    }
    if ( $id ) {
      $post_custom_css = get_post_meta( $id, '_marvelous_shortcodes_custom_css', true );
      if ( ! empty( $post_custom_css ) ) {
        $post_custom_css = strip_tags( $post_custom_css );
        echo '<style type="text/css" data-type="marvelous_custom-css">';
        echo $post_custom_css; // @codingStandardsIgnoreLine
        echo '</style>';
      }
    }
  }

  /**
   * Check if the plugin meets requirements and
   * disable it if they are not present.
   *
   * @since  1.0
   * @return boolean result of meets_requirements
   */
  public function check_requirements() {
    // bail early if pluginmeets requirements
    if ( $this->meets_requirements() ) {
      return true;
    }

    // Add a dashboard notice.
    add_action( 'all_admin_notices', array( $this, 'requirements_not_met_notice' ) );

    // Deactivate our plugin.
    add_action( 'admin_init', array( $this, 'deactivate_me' ) );

    return false;
  }

  /**
   * Deactivates this plugin, hook this function on admin_init.
   *
   * @since  1.0
   * @return void
   */
  public function deactivate_me() {
    // We do a check for deactivate_plugins before calling it, to protect
    // any developers from accidentally calling it too early and breaking things.
    if ( function_exists( 'deactivate_plugins' ) ) {
      deactivate_plugins( $this->basename );
    }
  }

  /**
   * Check that all plugin requirements are met
   *
   * @since  1.0
   * @return boolean True if requirements are met.
   */
  public function meets_requirements() {
    // Do checks for required classes / functions
    // function_exists('') & class_exists('').
    // We have met all requirements.
    // Add detailed messages to $this->activation_errors array

    // Check if Visual Composer is installed
    if ( ! defined( 'WPB_VC_VERSION' ) ) {
      return false;
    }

    return true;
  }

  /**
   * Adds a notice to the dashboard if the plugin requirements are not met
   *
   * @since  1.0
   * @return void
   */
  public function requirements_not_met_notice() {
    // compile default message
    $default_message = sprintf(
      __( 'Marvelous Hover is missing requirements and has been <a href="%s">deactivated</a>. Please make sure all requirements are available.', 'legocreative' ),
      admin_url( 'plugins.php' )
    );

    // default details to null
    $details = null;

    // add details if any exist
    if ( ! empty( $this->activation_errors ) && is_array( $this->activation_errors ) ) {
      $details = '<small>' . implode( '</small><br /><small>', $this->activation_errors ) . '</small>';
    }

    // output errors
    ?>
    <div id="message" class="error">
      <p><?php echo $default_message; // @codingStandardsIgnoreLine ?></p>
      <?php echo $details; // @codingStandardsIgnoreLine ?>
    </div>
    <?php


    if ( ! defined( 'WPB_VC_VERSION' ) ) {
      $plugin_data = get_plugin_data( __FILE__ );
      echo '
      <div class="error"><p>' .
      sprintf( __( '<strong>%s</strong> requires <strong><a href="https://codecanyon.net/item/visual-composer-page-builder-for-wordpress/242431?ref=themelego" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'legocreative' ), $plugin_data['Name'] ) . // @codingStandardsIgnoreLine
      '</p></div>';
    }
  }

  /**
   * Magic getter for our object.
   *
   * @since  1.0
   * @param string $field Field to get.
   * @throws Exception Throws an exception if the field is invalid.
   * @return mixed
   */
  public function __get( $field ) {
    if ( isset( $this->shortcodes[ $field ] ) ) {
      return $this->shortcodes[ $field ];
    }

    switch ( $field ) {
      case 'version':
        return self::VERSION;
      case 'basename':
      case 'url':
      case 'path':
      case 'options':
      default:
        throw new Exception( 'Invalid ' . __CLASS__ . ' property: ' . $field );
    }
  }

  /**
   * Get the plugin path.
   *
   * @access public
   * @since  1.0
   * @return string
   */
  public function plugin_path() {
    return untrailingslashit( plugin_dir_path( __FILE__ ) );
  } // End plugin_path()

  /**
   * Get asset dir
   *
   * @access public
   * @since  1.0
   * @return string
   */
  public function assets_dir() {
    return plugins_url( 'assets/', __FILE__ );
  }

  public function registerDefaultScripts() {
    wp_register_style( 'marvelous-style', marvelous_hover_assets( 'css/marvelous-hover.css' ), array(), $this->__get( 'version' ), false );

    // 3rd
    wp_register_script( 'waypoints', marvelous_hover_assets( 'js/waypoints.min.js' ), array( 'jquery' ), $this->__get( 'version' ), true );
    wp_register_style( 'animate-css', marvelous_hover_assets( 'css/animate.min.css' ), array(), $this->__get( 'version' ), false );
    wp_register_style( 'prettyphoto', marvelous_hover_assets( 'css/prettyPhoto.min.css' ), array(), $this->__get( 'version' ), false );
    wp_register_script( 'prettyphoto', marvelous_hover_assets( 'js/jquery.prettyPhoto.min.js' ), array( 'jquery' ), $this->__get( 'version' ), true );

    // Row
    wp_register_script( 'marvelous-js', marvelous_hover_assets( 'js/marvelous-hover.js' ), array( 'jquery', 'waypoints' ), $this->__get( 'version' ) );
  }

  public function enqueueDefaultStyle() {
    wp_enqueue_style( 'marvelous-style' );
  }

  public function enqueueDefaultScripts() {
    if ( false === self::$default_scripts_enqueued ) {
      wp_enqueue_script( 'waypoints' );
      wp_enqueue_script( 'marvelous-js' );
      self::$default_scripts_enqueued = true;
    }
  }

  public function registerMetabox() {
    add_action( 'cmb2_admin_init', 'marvelous_hover_register_metabox' );
  }

  /**
   * Wrapper function around cmb2_get_option
   * @since  0.1.0
   * @param  string $key     Options array key
   * @param  mixed  $default Optional default value
   * @return mixed           Option value
   */
  function getOption( $key = '', $default = null ) {
    if ( function_exists( 'cmb2_get_option' ) ) {
      // Use cmb2_get_option as it passes through some key filters.
      return cmb2_get_option( $this->options->key, $key, $default );
    }
    // Fallback to get_option if CMB2 is not loaded yet.
    $opts = get_option( $this->options->key, $key, $default );
    $val = $default;
    if ( 'all' == $key ) {
      $val = $opts;
    } elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
      $val = $opts[ $key ];
    }
    return $val;
  }

  /**
   * Notices
   * Activation instructions & CodeCanyon rating notices START
   *
   * For theme developers who want to include our plugin, they will need
   * to disable this section. This can be done by include this line
   * in their theme:
   *
   * defined( 'LEGOCREATIVE_DISABLE_RATING_NOTICE' ) or define( 'LEGOCREATIVE_DISABLE_RATING_NOTICE', true );
   *
   * @return void
   */
  public function notices() {
    add_action( 'admin_notices', array( $this, 'remindSettingsAndSupport' ) );

    if ( ! defined( 'LEGOCREATIVE_DISABLE_RATING_NOTICE' ) ) {
      add_action( 'admin_notices', array( $this, 'remindRating' ) );
      add_action( 'wp_ajax_' . __CLASS__ . '-ask-rate', array( $this, 'ajaxRemindHandler' ) );
    }
  }

  /**
   * Activate the plugin
   *
   * @since  1.0
   * @return void
   */
  public function _activate() {
    // Make sure any rewrite functionality has been loaded.
    flush_rewrite_rules();

    delete_transient( __CLASS__ . '-activated' );
    set_transient( __CLASS__ . '-activated', time(), MINUTE_IN_SECONDS * 2 );

    if ( ! defined( 'LEGOCREATIVE_DISABLE_RATING_NOTICE' ) ) {
      delete_transient( __CLASS__ . '-ask-rate' );
      set_transient( __CLASS__ . '-ask-rate', time(), DAY_IN_SECONDS * 4 );

      update_option( __CLASS__ . '-ask-rate-placeholder', 1 );
    }
  }

  /**
   * Deactivate the plugin
   * Uninstall routines should be in uninstall.php
   *
   * @since  1.0
   * @return void
   */
  public function _deactivate() {
    delete_transient( __CLASS__ . '-activated' );
    delete_transient( __CLASS__ . '-ask-rate' );
    delete_option( __CLASS__ . '-ask-rate-placeholder' );
  }

  /**
   * Ajax handler for when a button is clicked in the 'ask rating' notice
   *
   * @return  void
   * @since 1.0
   **/
  public function ajaxRemindHandler() {
    check_ajax_referer( __CLASS__, '_nonce' );

    if ( isset( $_POST['type'] ) ) {
      if ( 'remove' == $_POST['type'] ) {
        delete_option( __CLASS__ . '-ask-rate-placeholder' );
      } else { // remind
        set_transient( __CLASS__ . '-ask-rate', time(), DAY_IN_SECONDS );
      }
    }

    die();
  }

  /**
   * Displays the notice for reminding the user to rate our plugin
   *
   * @since   1.0
   *
   * @return  void
   **/
  public function remindRating() {
    if ( get_option( __CLASS__ . '-ask-rate-placeholder' ) === false ) {
      return;
    }
    if ( get_transient( __CLASS__ . '-ask-rate' ) ) {
      return;
    }

    $pluginData = get_plugin_data( __FILE__ );
    $nonce = wp_create_nonce( __CLASS__ );

    // @codingStandardsIgnoreStart
    echo '<div class="updated legocreative-ask-rating" style="border-left-color: #3498db">
      <p>
        <img src="' . esc_url( marvelous_hover_assets( 'img/logo.jpg' ) ) . '" style="display: block; margin-bottom: 10px"/>
        <strong>' . sprintf( __( 'Enjoying %s?', $this->basename ), $pluginData['Name'] ) . '</strong><br>' .
        __( 'Help us out by rating our plugin 5 stars in CodeCanyon! This will allow us to create more awesome products and provide top notch customer support.', $this->basename ) . '<br>' .
        '<button data-href="http://codecanyon.net/downloads?utm_source=' .
        urlencode( $pluginData['Name'] ) . '&utm_medium=rate_notice" class="button button-primary" style="margin: 10px 10px 10px 0;">' .
        __( 'Rate us 5 stars in CodeCanyon :)', $this->basename ) .
        '</button>' .
        '<button class="button button-secondary remind" style="margin: 10px 10px 10px 0;">' . __( 'Remind me tomorrow', $this->basename ) . '</button>' .
        '<button class="button button-secondary nothanks" style="margin: 10px 0;">' . __( 'I&apos;ve already rated!', $this->basename ) . '</button>' .
        '<script>
        jQuery(document).ready(function($) {
          "use strict";

          $(".legocreative-ask-rating button").click(function() {
            if ( $(this).is(".button-primary") ) {
              var $this = $(this);

              var data = {
                "_nonce": "' . $nonce . '",
                "action": "' . __CLASS__ . '-ask-rate",
                "type": "remove"
              };

              $.post(ajaxurl, data, function(response) {
                $this.parents(".updated:eq(0)").fadeOut();
                window.open($this.attr("data-href"), "_blank");
              });

            } else if ( $(this).is(".remind") ) {
              var $this = $(this);

              var data = {
                "_nonce": "' . $nonce . '",
                "action": "' . __CLASS__ . '-ask-rate",
                "type": "remind"
              };

              $.post(ajaxurl, data, function(response) {
                $this.parents(".updated:eq(0)").fadeOut();
              });

            } else if ( $(this).is(".nothanks") ) {
              var $this = $(this);

              var data = {
                "_nonce": "' . $nonce . '",
                "action": "' . __CLASS__ . '-ask-rate",
                "type": "remove"
              };

              $.post(ajaxurl, data, function(response) {
                $this.parents(".updated:eq(0)").fadeOut();
              });
            }
            return false;
          });
        });
        </script>
      </p>
    </div>';
    // @codingStandardsIgnoreEnd
  }

  /**
   * Displays the notice that we have a support site and additional instructions
   *
   * @return  void
   * @since 1.0
   **/
  public function remindSettingsAndSupport() {
    if ( ! get_transient( __CLASS__ . '-activated' ) ) {
      return;
    }

    $pluginData = get_plugin_data( __FILE__ );

    // @codingStandardsIgnoreStart
    echo '<div class="updated" style="border-left-color: #3498db">
      <p>
        <img src="' . esc_url( marvelous_hover_assets( 'img/logo.jpg' ) ) . '" style="display: block; margin-bottom: 10px"/>
        <strong>' . sprintf( __( 'Thank you for activating %s!', $this->basename ), $pluginData['Name'] ) . '</strong><br>' .

        // Tell users how to use the plugin.
        __( 'Now just edit your pages, add shortcode in content list, and style it, all in tinyMCE editor.', $this->basename ) . '<br>' .

        __( 'If you need any support, you can leave us a ticket in our support site. The link to our support site is also listed in the plugin details for future reference.', $this->basename ) . '<br>' .
        '<a href="http://support.themelego.com?utm_source=' . urlencode( $pluginData['Name'] ) . '&utm_medium=activation_notice" class="legocreative_ask_rate button button-default" style="margin: 10px 0;" target="_blank">' . __( 'Visit our support site', $this->basename ) . '</a>' .
        ' or send us an email to <a href="mailto:support@themelego.com">support@themelego.com</a>' .
        '<br>' .
        '<em style="color: #999">' . __( 'This notice will go away in a moment', $this->basename ) . '</em><br>
      </p>
    </div>';
    // @codingStandardsIgnoreEnd
  }

  /**
   * Check if visual composer installed
   *
   * @since 2.0
   */
  public function has_visual_composer() {
    if ( defined( 'WPB_VC_VERSION' ) && function_exists ( 'vc_add_shortcode_param' ) ) {
      return true;
    }

    return false;
  }

  /**
   * Wordpres version
   * integrate with tinyMCE
   *
   * @since 2.0
   */
  public function intergrateTinyMCE() {
    if ( is_admin() ) {
      add_action( 'admin_head', array( $this, 'admin_head' ) );
    }
  }

  /**
   * admin_head
   * calls your functions into the correct filters
   *
   * @since  2.0
   * @return void
   */
  function admin_head() {
    // check user permissions
    if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
      return;
    }

    // check if WYSIWYG is enabled
    if ( 'true' == get_user_option( 'rich_editing' ) ) {
      add_filter( 'mce_external_plugins', array( $this, 'mce_external_plugins' ) );
      add_filter( 'mce_buttons', array( $this, 'mce_buttons' ) );
    }
  }

  /**
   * mce_external_plugins
   * Adds our tinymce plugin
   *
   * @since  2.0
   * @param  array $plugin_array
   * @return array
   */
  function mce_external_plugins( $plugin_array ) {
    $plugin_array[ $this->tinymce_tag ] = marvelous_hover_assets( 'js/marvelous-editor.js' );
    return $plugin_array;
  }

  /**
   * mce_buttons
   * Adds our tinymce button
   *
   * @since  2.0
   * @param  array $buttons
   * @return array
   */
  function mce_buttons( $buttons ) {
    array_push( $buttons, $this->tinymce_tag );
    return $buttons;
  }

  public function getMarvelousShortcode() {
    if ( isset( $_GET['name'] ) ) {
      $this->getShortcodeForm( $_GET['name'] ); //@codingStandardsIgnoreLine
    }

    die();
  }

  public function getShortcodeForm( $name ) {
    $shortcode = $this->__get( $name );
    if ( isset( $shortcode ) ) {
      $shortcode_setting = $shortcode->params();
      $params = $shortcode_setting['params'];
      $form_template = MARVELOUS_PLUGIN_DIR . '/inc/editor-form.php';

      ob_start();
      include( $form_template );
      $output = ob_get_contents();
      ob_end_clean();

      echo $output; //@codingStandardsIgnoreLine
    }
    die;
  }
}

// Kick it off.
add_action( 'plugins_loaded', array( marvelous_hover(), 'hooks' ) );

register_activation_hook( __FILE__, array( marvelous_hover(), '_activate' ) );
register_deactivation_hook( __FILE__, array( marvelous_hover(), '_deactivate' ) );
