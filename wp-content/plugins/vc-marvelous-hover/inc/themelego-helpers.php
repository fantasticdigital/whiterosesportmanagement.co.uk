<?php
if ( ! defined( 'ABSPATH' ) ) {
  die( '-1' );
}

if ( ! function_exists( 'marvelous_map_get_attributes' ) ) {
  /**
   * Pack from vc_map_get_attributes
   *
   * @since 2.0
   *
   * @param $tag - shortcode tag3
   * @param $atts - shortcode attributes
   *
   * @return array - return merged values with provided attributes ( 'a'=>1,'b'=>2 + 'b'=>3,'c'=>4 --> 'a'=>1,'b'=>3 )
   *
   * @see vc_shortcode_attribute_parse - return union of provided attributes ( 'a'=>1,'b'=>2 + 'b'=>3,'c'=>4 --> 'a'=>1,
   *     'b'=>3, 'c'=>4 )
   */
  function marvelous_map_get_attributes( $tag, $atts = array() ) {
    return shortcode_atts( marvelous_map_get_defaults( $tag ), $atts, $tag );
  }
}

if ( ! function_exists( 'marvelous_map_get_defaults' ) ) {
  /**
   * Pack from vc_map_get_defaults
   *
   * @since 2.0
   *
   * Function to get defaults values for shortcode.
   *
   * @param $tag - shortcode tag
   *
   * @return array - list of param=>default_value
   */
  function marvelous_map_get_defaults( $tag ) {
    $params = array();
    $shortcode = marvelous_get_shortcode( $tag );

    if ( isset( $shortcode ) ) {
      $shortcode_setting = $shortcode->params();

      if ( isset( $shortcode_setting ) ) {
        $params = marvelous_map_get_params_defaults( $shortcode_setting['params'] );
      }
    }

    return $params;
  }
}

if ( ! function_exists( 'marvelous_map_get_params_defaults' ) ) {
  /**
   * Pack from vc_map_get_params_defaults
   * @since 2.0
   *
   * @param $params
   * @return array
   */
  function marvelous_map_get_params_defaults( $params ) {
    $resultParams = array();
    foreach ( $params as $param ) {
      if ( isset( $param['param_name'] ) && 'content' !== $param['param_name'] ) {
        $value = '';
        if ( isset( $param['std'] ) ) {
          $value = $param['std'];
        } elseif ( isset( $param['value'] ) ) {
          if ( is_array( $param['value'] ) ) {
            $value = current( $param['value'] );
            if ( is_array( $value ) ) {
              // in case if two-dimensional array provided (vc_basic_grid)
              $value = current( $value );
            }
            // return first value from array (by default)
          } else {
            $value = $param['value'];
          }
        }
        $resultParams[ $param['param_name'] ] = apply_filters( 'marvelous_map_get_param_defaults', $value, $param );
      }
    }

    return $resultParams;
  }
}

if ( ! function_exists( 'marvelous_get_shortcode' ) ) {
  /**
   * Get shortcode object
   *
   * @since 2.0
   *
   * @param $tag | shortcode string
   * @return Marvelous Shortcode Item
   */
  function marvelous_get_shortcode( $tag ) {
    return marvelous_hover()->__get( $tag );
  }
}

if ( ! function_exists( 'marvelous_value_from_safe' ) ) {
  /**
   * @param $value
   * @param bool $encode
   *
   * @since 2.0
   * @return string
   */
  function marvelous_value_from_safe( $value, $encode = false ) {
    $value = preg_match( '/^#E\-8_/', $value ) ? rawurldecode( base64_decode( preg_replace( '/^#E\-8_/', '', $value ) ) ) : $value;
    if ( $encode ) {
      $value = htmlentities( $value, ENT_COMPAT, 'UTF-8' );
    }

    return $value;
  }
}

if ( ! function_exists( 'marvelous_taxonomies_types' ) ) {
  /**
   * Marvelous taxonomies types
   * Pack from visual composer
   *
   * @since 2.0
   */
  function marvelous_taxonomies_types() {
    global $marvelous_taxonomies_types;
    if ( is_null( $marvelous_taxonomies_types ) ) {
      $marvelous_taxonomies_types = get_taxonomies( array( 'public' => true ), 'objects' );
    }

    return $marvelous_taxonomies_types;
  }
}

if ( ! function_exists( 'marvelous_post_param' ) ) {
  /**
   * Pack from visual composer
   *
   * Get param value from $_POST if exists.
   *
   * @param $param
   * @param $default
   *
   * @since 2.0
   * @return null|string - null for undefined param.
   */
  function marvelous_post_param( $param, $default = null ) {
    return isset( $_POST[ $param ] ) ? $_POST[ $param ] : $default; //@codingStandardsIgnoreLine
  }
}

if ( ! function_exists( 'marvelous_build_link' ) ) {
  /**
   * Pack from visual composer
   *
   * @param $param
   * @param $default
   *
   * @since 2.0
   */
  function marvelous_build_link( $value, $default = null ) {
    if ( preg_match( '/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}((:[0-9]{1,5})?\\/.*)?$/i', $value ) ) {
      return array(
        'url' => $value,
      );
    }

    $result = $default;
    $params_pairs = explode( '|', $value );
    if ( ! empty( $params_pairs ) ) {
      foreach ( $params_pairs as $pair ) {
        $param = preg_split( '/\:/', $pair );
        if ( ! empty( $param[0] ) && isset( $param[1] ) ) {
          $result[ $param[0] ] = rawurldecode( $param[1] );
        }
      }
    }

    return $result;
  }
}

if ( ! function_exists( 'marvelous_get_dropdown_option' ) ) {
  /**
   * Pack from visual composer
   *
   * @param $param
   * @param $value
   *
   * @since 2.0
   * @return mixed|string
   */
  function marvelous_get_dropdown_option( $param, $value ) {
    if ( '' === $value && is_array( $param['value'] ) ) {
      $value = array_shift( $param['value'] );
    }
    if ( is_array( $value ) ) {
      reset( $value );
      $value = isset( $value['value'] ) ? $value['value'] : current( $value );
    }
    $value = preg_replace( '/\s/', '_', $value );

    return ( '' !== $value ? $value : '' );
  }
}

add_filter( 'marvelous_map_get_param_defaults', 'marvelous_checkbox_param_defaults', 10, 2 );
if ( ! function_exists( 'marvelous_checkbox_param_defaults' ) ) {
  function marvelous_checkbox_param_defaults( $value, $param ) {
    if ( 'checkbox' === $param['type'] ) {
      $value = '';
      if ( isset( $param['std'] ) ) {
        $value = $param['std'];
      }
    }

    return $value;
  }
}
