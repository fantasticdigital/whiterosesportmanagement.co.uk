<?php
if ( ! defined( 'ABSPATH' ) ) {
  die( '-1' );
}

/**
 * Core functions
 */

/**
 * Grab the VCMarvelousHover object and return it.
 * Wrapper for VCMarvelousHover::get_instance()
 *
 * @since  1.0
 * @return VCMarvelousHover  Singleton instance of plugin class.
 */
function marvelous_hover() {
  return VCMarvelousHover::get_instance();
}

if ( ! function_exists( 'marvelous_hover_assets' ) ) {
  /**
   * Get plugin assets
   *
   * @since 1.0
   *
   * @param string - full directory path to assets
   */
  function marvelous_hover_assets( $file ) {
    return marvelous_hover()->assets_dir() . $file;
  }
}

if ( ! function_exists( 'marvelous_get_option' ) ) {
  function marvelous_get_option( $key = '', $default = null ) {
    return marvelous_hover()->getOption( $key, $default );
  }
}

if ( ! function_exists( 'marvelous_has_visual_commposer' ) ) {
  function marvelous_has_visual_commposer() {
    return marvelous_hover()->has_visual_composer();
  }
}

