<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

/**
 * Marverlous Hover Shortcode Sample class.
 *
 * @since 1.0
 * @package Marvelous Hover Effect
 */

class Marvelous_Shortcode_Image_Tilt extends Marvelous_Shortcode_Base {

  protected $shortcode = 'marvelous_image_tilt';

  public function params() {
    return array(
      'name'        => __( 'Marvelous Image Tilt', 'legocreative' ),
      'description'  => __( 'Tilt effect for images', 'legocreative' ),
      'base'        => $this->shortcode,
      'icon' => marvelous_hover_assets( 'img/icon.png' ),
      'class'       => '',
      'params'      => array(
        array(
          'type' => 'dropdown',
          'heading' => __( 'Image ratio', 'legocreative' ),
          'param_name' => 'ratio',
          'value' => array(
            __( '3:4', 'legocreative' )        => '34',
            __( '4:3', 'legocreative' )        => '43',
            __( '1:1 Square', 'legocreative' ) => '11',
            __( '16:9', 'legocreative' )       => '169',
            __( '3:2', 'legocreative' )        => '32',
            __( '2:1', 'legocreative' )        => '21',
            __( '1:2', 'legocreative' )        => '12',
          ),
          'description' => __( 'Image tilt require the image ratio to show image, (We do not show the original image size).', 'legocreative' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Style', 'legocreative' ),
          'param_name' => 'style',
          'value'      => array(
            __( 'Default', 'legocreative' ) => 'style0',
            __( 'Style 1', 'legocreative' ) => 'style1',
            __( 'Style 2', 'legocreative' ) => 'style2',
            __( 'Style 3', 'legocreative' ) => 'style3',
            __( 'Style 4', 'legocreative' ) => 'style4',
            __( 'Style 5', 'legocreative' ) => 'style5',
            __( 'Style 6', 'legocreative' ) => 'style6',
            __( 'Style 7', 'legocreative' ) => 'style7',
            __( 'Style 8', 'legocreative' ) => 'style8',
            __( 'Style 9', 'legocreative' ) => 'style9',
            __( 'Style 10', 'legocreative' ) => 'style10',
            __( 'Custom movement', 'legocreative' ) => 'custom',
          ),
          'description' => __( 'Select the movement style', 'legocreative' ),
        ),
        array(
          'type' => 'attach_image',
          'heading' => __( 'Image', 'legocreative' ),
          'param_name' => 'image',
          'description' => __( 'Select your image to apply effect', 'legocreative' ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Extra images', 'legocreative' ),
          'param_name' => 'extra_imgs',
          'std' => '2',
          'description' => __( 'Number of extra background-image divisions; min: 0, max: 64', 'legocreative' ),
          'dependency'  => array(
            'element' => 'style',
            'value'   => array( 'custom' ),
          ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Opacity', 'legocreative' ),
          'param_name' => 'opacity',
          'std' => '0.7',
          'description' => __( 'The opacity value for the background-image divisions', 'legocreative' ),
          'dependency'  => array(
            'element' => 'style',
            'value'   => array( 'custom' ),
          ),
        ),
        array(
          'type' => 'checkbox',
          'heading' => __( 'Visible when overflow', 'legocreative' ),
          'param_name' => 'overflow',
          'value' => array(
            __( 'Yes', 'legocreative' ) => 'true',
          ),
          'std' => 'true',
          'description' => __( 'Visible image when overflow', 'legocreative' ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Perspective', 'legocreative' ),
          'param_name' => 'perspective',
          'group' => __( 'Movement', 'legocreative' ),
          'std' => '1000',
          'description' => __( 'The perspective value for the 3D transforms', 'legocreative' ),
          'dependency'  => array(
            'element' => 'style',
            'value'   => array( 'custom' ),
          ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Translate X', 'legocreative' ),
          'param_name' => 'translate_x',
          'group' => __( 'Movement', 'legocreative' ),
          'std' => '10',
          'description' => __( 'The relative movement on the x-axis', 'legocreative' ),
          'dependency'  => array(
            'element' => 'style',
            'value'   => array( 'custom' ),
          ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Translate Y', 'legocreative' ),
          'param_name' => 'translate_y',
          'group' => __( 'Movement', 'legocreative' ),
          'std' => '10',
          'description' => __( 'The relative movement on the y-axis', 'legocreative' ),
          'dependency'  => array(
            'element' => 'style',
            'value'   => array( 'custom' ),
          ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Translate Z', 'legocreative' ),
          'param_name' => 'translate_z',
          'group' => __( 'Movement', 'legocreative' ),
          'std' => '10',
          'description' => __( 'The relative movement on the z-axis', 'legocreative' ),
          'dependency'  => array(
            'element' => 'style',
            'value'   => array( 'custom' ),
          ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Rotate X', 'legocreative' ),
          'param_name' => 'rotate_x',
          'group' => __( 'Movement', 'legocreative' ),
          'std' => '5',
          'description' => __( 'The relative rotation on the x-axis. A perspective value needs to be set', 'legocreative' ),
          'dependency'  => array(
            'element' => 'style',
            'value'   => array( 'custom' ),
          ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Rotate Y', 'legocreative' ),
          'param_name' => 'rotate_y',
          'group' => __( 'Movement', 'legocreative' ),
          'std' => '5',
          'description' => __( 'The relative rotation on the y-axis. A perspective value needs to be set', 'legocreative' ),
          'dependency'  => array(
            'element' => 'style',
            'value'   => array( 'custom' ),
          ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Rotate Z', 'legocreative' ),
          'param_name' => 'rotate_z',
          'group' => __( 'Movement', 'legocreative' ),
          'std' => '5',
          'description' => __( 'The relative rotation on the z-axis. A perspective value needs to be set', 'legocreative' ),
          'dependency'  => array(
            'element' => 'style',
            'value'   => array( 'custom' ),
          ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Scale grade', 'legocreative' ),
          'param_name' => 'extra_imgs_scale_grade',
          'group' => __( 'Advanced', 'legocreative' ),
          'std' => '0',
          'description' => __( 'Float number for set scale gradient. This option create effect like tunnel or add more perspective', 'legocreative' ),
          'dependency'  => array(
            'element' => 'style',
            'value'   => array( 'custom' ),
          ),
        ),
        array(
          'type' => 'checkbox',
          'heading' => __( 'First layer not move', 'legocreative' ),
          'param_name' => 'bgfixed',
          'value' => array(
            __( 'Yes', 'legocreative' ) => 'true',
          ),
          'group' => __( 'Advanced', 'legocreative' ),
          'description' => __( 'By default it&#39s moving', 'legocreative' ),
          'dependency'  => array(
            'element' => 'style',
            'value'   => array( 'custom' ),
          ),
        ),
        array(
          'type'         => 'textfield',
          'heading'      => __( 'Extra class name', 'legocreative' ),
          'param_name'   => 'el_class',
          'description'  => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.
', 'legocreative' ),
          ),

        array(
          'type' => 'css_editor',
          'heading' => __( 'CSS box', 'legocreative' ),
          'param_name' => 'css',
          'group' => __( 'Design Options', 'legocreative' ),
        ),
      ),
    );
  }
}

if ( class_exists( 'WPBakeryShortCode' ) ) {
  class WPBakeryShortCode_Marvelous_Image_Tilt_Effect extends WPBakeryShortCode {}
}
