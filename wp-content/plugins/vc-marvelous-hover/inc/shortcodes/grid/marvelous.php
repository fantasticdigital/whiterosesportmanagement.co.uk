<?php
if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly.
}

/**
 * Marvelous Shortcode Grid Item class.
 *
 * @since 1.0
 * @package Marvelous Hover Effects
 */

class Marvelous_Shortcode_Grid_Item extends Marvelous_Shortcode_Grid {

  protected $grid = 'marvelous';

  /**
   * Constructor
   *
   * @since  1.0
   * @param  VCMarvelousHover $plugin Main plugin object.
   * @return void
   */
  public function __construct( $plugin ) {
    $this->shortcode = 'marvelous_grid';
    parent::__construct( $plugin );
  }

  public function params() {
    return array(
      'name' => __( 'Marvelous Grid', 'legocreative' ),
      'description' => __( 'Marvelous hover effects grid.', 'legocreative' ),
      'base' => $this->shortcode,
      'icon' => marvelous_hover_assets( 'img/icon.png' ),
      'params' => array_merge( array(
        array(
          'type'       => 'dropdown',
          'heading'    => __( 'Item uppear effect', 'legocreative' ),
          'param_name' => 'grid_effect',
          'value'      => array(
            __( 'Fade in', 'legocreative' )   => 'effect-1',
            __( 'Move up', 'legocreative' )   => 'effect-2',
            __( 'Scale up', 'legocreative' )   => 'effect-3',
            __( 'Fall Perspective', 'legocreative' )   => 'effect-4',
            __( 'Fly', 'legocreative' )   => 'effect-5',
            __( 'Flip', 'legocreative' )   => 'effect-6',
            __( 'Helix', 'legocreative' )   => 'effect-7',
            __( 'Popup', 'legocreative' )   => 'effect-8',
          ),
          'std'         => 'effect-1',
          'description' => __( 'Select animate grid loading effect.', 'legocreative' ),
        ),
      ), $this->getBasicAtts(), array(
        // Marvelous tab
        array(
          'type' => 'dropdown',
          'heading' => __( 'Square ratio', 'legocreative' ),
          'param_name' => 'ratio',
          'value' => array(
            __( 'Original', 'legocreative' )   => 'original',
            __( '4:3', 'legocreative' )        => '43',
            __( '1:1 Square', 'legocreative' ) => '11',
            __( '16:9', 'legocreative' )       => '169',
            __( '3:2', 'legocreative' )        => '32',
            __( '2:1', 'legocreative' )        => '21',
            __( '3:4', 'legocreative' )        => '34',
          ),
          //'std' => '43',
          'group' => __( 'Marvelous hover', 'legocreative' ),
          'description' => __( 'Select image size.', 'legocreative' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Effect', 'legocreative' ),
          'param_name' => 'effect',
          'value' => $this->getEffects(),
          'std' => 'lily',
          'group' => __( 'Marvelous hover', 'legocreative' ),
          'description' => __( 'Select hover effect you want to display.', 'legocreative' ),
        ),
        array(
          'type' => 'checkbox',
          'heading' => __( 'Override post effect?', 'legocreative' ),
          'param_name' => 'disable_item_effect',
          'value' => array( __( 'Yes', 'legocreative' ) => 'yes' ),
          'group' => __( 'Marvelous hover', 'legocreative' ),
          'description' => __( 'If you choose this option, all item will has same effect. If not each item has effect define by its self', 'legocreative' ),
        ),
        array(
          'type' => 'checkbox',
          'heading' => __( 'Use theme default font family?', 'legocreative' ),
          'param_name' => 'disable_font',
          'value' => array( __( 'Yes', 'legocreative' ) => 'yes' ),
          'group' => __( 'Marvelous hover', 'legocreative' ),
          'description' => __( 'Use font family from the theme.', 'legocreative' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'On click action (enable link)', 'js_composer' ),
          'param_name' => 'enable_link',
          'group' => __( 'Marvelous hover', 'legocreative' ),
          'value' => array(
            __( 'Open post detail', 'legocreative' ) => 'yes',
            __( 'Open prettyPhoto', 'legocreative' ) => 'link_image',
          ),
          'description' => __( 'Select action for click action.', 'legocreative' ),
        ),
        array(
          'type'        => 'checkbox',
          'heading'     => __( 'Open link in new tab', 'legocreative' ),
          'param_name'  => 'link_new_tab',
          'group' => __( 'Marvelous hover', 'legocreative' ),
          'value'       => array( __( 'Yes', 'legocreative' ) => 'yes' ),
          'description' => __( 'Enable this option to open link in new tab', 'legocreative' ),
          'dependency'  => array(
            'element' => 'enable_link',
            'value'   => array( 'yes' ),
          ),
        ),

        array(
          'type' => 'textfield',
          'heading' => __( 'Image opacity', 'legocreative' ),
          'param_name' => 'image_opacity',
          'group'      => __( 'Customize', 'legocreative' ),
          'description' => __( 'Marvelous hover set opacity on Image to create the overlay. Default: 0.8 and depends on hover effect', 'legocreative' ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Hover image opacity', 'legocreative' ),
          'param_name' => 'hover_image_opacity',
          'group'      => __( 'Customize', 'legocreative' ),
          'description' => __( 'Choose hover image opacity. Default depends on hover effect', 'legocreative' ),
        ),
        array(
          'type'       => 'colorpicker',
          'heading'    => __( 'Text Color', 'legocreative' ),
          'param_name' => 'color',
          'group'      => __( 'Customize', 'legocreative' ),
          'description' => __( 'Select color for heading and paragraph.', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-6',
        ),
        array(
          'type'       => 'colorpicker',
          'heading'    => __( 'Hover text color', 'legocreative' ),
          'param_name' => 'hover_color',
          'group'      => __( 'Customize', 'legocreative' ),
          'description' => __( 'Select color for when hover in.', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-6',
        ),
        array(
          'type'       => 'colorpicker',
          'heading'    => __( 'Overlay color', 'legocreative' ),
          'param_name' => 'overlay_color',
          'group'      => __( 'Customize', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-6',
        ),
        array(
          'type'       => 'colorpicker',
          'heading'    => __( 'Hover overlay color', 'legocreative' ),
          'param_name' => 'hover_overlay_color',
          'group'      => __( 'Customize', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-6',
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Heading font size', 'legocreative' ),
          'group'      => __( 'Customize', 'legocreative' ),
          'param_name' => 'heading_font_size',
          'description' => __( 'Choose heading font size', 'legocreative' ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Description font size', 'legocreative' ),
          'group'      => __( 'Customize', 'legocreative' ),
          'param_name' => 'description_font_size',
          'description' => __( 'Choose description font size', 'legocreative' ),
        ),
        array(
          'type'       => 'marvelous_class',
          'heading'    => '',
          'param_name' => 'custom_class',
          'group'      => __( 'Customize', 'legocreative' ),
          'value'      => '',
        ),
      ) ),
    );
  }

  public function getSupportedPostTypes() {
    return apply_filters( 'marvelous_hover_post_types', $this->getPostTypeList() );
  }

  public function getCustomCSS( $atts ) {
    return $this->getMarvelousCustomStyle( 'grid', $atts );
  }
}

if ( class_exists( 'WPBakeryShortCode' ) ) {
  class WPBakeryShortCode_Marvelous_Grid extends WPBakeryShortCode {}
}
