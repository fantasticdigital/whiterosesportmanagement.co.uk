<?php
if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly.
}

/**
 * Marvelous Shortcode Tilt Hover class.
 *
 * @since 1.3
 * @package Marvelous Hover Effect
 */
class Marvelous_Shortcode_Tilt extends Marvelous_Shortcode_Base {

  /**
   * Image size
   *
   * @since 1.3
   */
  public $image_size = 'marvelous_tilt_size';
  public $image_size_width = 600;
  public $image_size_height = 830;

  protected $shortcode = 'marvelous_tilt';

  /**
   * Override parent constructor
   *
   * @since  1.3
   * @param  VCMarvelousHover $plugin Main plugin object.
   * @return void
   */
  public function __construct( $plugin ) {
    parent::__construct( $plugin );

    if ( 0 !== marvelous_get_option( 'marvelous_enable_image_size' ) ) {
      $this->registerImageSize();
    }
  }

  /**
   * Register image sizes
   *
   * @since 1.3
   * @return void
   */
  public function registerImageSize() {
    add_image_size( $this->image_size, $this->image_size_width, $this->image_size_height, true );
  }

  public function params() {
    return array(
      'name'        => __( 'Marvelous Tilt', 'legocreative' ),
      'description' => __( 'Marvelous hover with a tilt effect', 'legocreative' ),
      'base'        => $this -> shortcode,
      'icon' => marvelous_hover_assets( 'img/icon.png' ),
      'class'       => '',
      'params'      => array(
        array(
          'type'       => 'dropdown',
          'heading'    => __( 'Style', 'legocreative' ),
          'param_name' => 'style',
          'value'      => array(
              __( 'Style 1', 'legocreative' )   => '1',
              __( 'Style 2', 'legocreative' )   => '2',
              __( 'Style 3', 'legocreative' )   => '3',
              __( 'Style 4', 'legocreative' )   => '4',
              __( 'Style 5', 'legocreative' )   => '5',
              __( 'Style 6', 'legocreative' )   => '6',
              __( 'Style 7', 'legocreative' )   => '7',
              __( 'Style 8', 'legocreative' )   => '8',
          ),
          'std' => '1',
          'description' => __( "Select icon type. If you don't want to use icon, you can use your image", 'legocreative' ),
        ),
        array(
          'type'        => 'attach_image',
          'heading'     => 'Image',
          'param_name'  => 'image',
          'description' => __( 'Select image from media library for effect.', 'legocreative' ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Title', 'legocreative' ),
          'param_name' => 'title',
          'value' => '',
          'description' => __( 'Choose your title', 'legocreative' ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Sub title', 'legocreative' ),
          'param_name' => 'sub_title',
          'value' => '',
          'description' => __( 'Choose your sub title', 'legocreative' ),
        ),
        array(
          'type' => 'checkbox',
          'heading' => __( 'Use theme default title font family?', 'legocreative' ),
          'param_name' => 'use_theme_fonts',
          'value' => array( __( 'Yes', 'legocreative' ) => 'yes' ),
          'std' => 'yes',
          'description' => __( 'Use font family from the theme.', 'legocreative' ),
        ),
        array(
          'type' => 'google_fonts',
          'param_name' => 'google_fonts',
          'value' => 'font_family:Abril%20Fatface%3Aregular|font_style:400%20regular%3A400%3Anormal',
          'settings' => array(
            'fields' => array(
              'font_family_description' => __( 'Select font family for title.', 'legocreative' ),
              'font_style_description' => __( 'Select font styling for title.', 'legocreative' ),
            ),
          ),
          'dependency' => array(
            'element' => 'use_theme_fonts',
            'value_not_equal_to' => 'yes',
          ),
        ),
        array(
          'type'        => 'textfield',
          'heading'     => __( 'Title fonts size', 'legocreative' ),
          'param_name'  => 'title_size',
        ),
        array(
          'type'        => 'textfield',
          'heading'     => __( 'Subtitle fonts size', 'legocreative' ),
          'param_name'  => 'sub_title_size',
        ),
        array(
          'type' => 'checkbox',
          'heading' => __( 'Enable moving smoothly', 'legocreative' ),
          'param_name' => 'transition',
          'value' => array( __( 'Yes', 'legocreative' ) => 'yes' ),
          'description' => __( 'Smooth hover with transition', 'legocreative' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Image ratio', 'legocreative' ),
          'param_name' => 'ratio',
          'value' => array(
            __( 'Custom (300x415)', 'legocreative' )        => 'custom',
            __( 'Original', 'legocreative' )   => 'original',
          ),
          'description' => __( 'Select image ratio.', 'legocreative' ),
        ),
        array(
          'type'             => 'colorpicker',
          'heading'          => __( 'Primary color', 'legocreative' ),
          'param_name'       => 'color',
          'description'      => __( 'Choose the primary color for title, sub titlle, frame.', 'legocreative' ),
        ),
        array(
          'type'             => 'colorpicker',
          'heading'          => __( 'Overlay start color', 'legocreative' ),
          'param_name'       => 'overlay_start',
          'description'      => __( 'Choose overlay start color.', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-4',
        ),
        array(
          'type'             => 'colorpicker',
          'heading'          => __( 'Overlay end color', 'legocreative' ),
          'param_name'       => 'overlay_end',
          'description'      => __( 'Choose overlay end color.', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-4',
        ),
        array(
          'type'        => 'textfield',
          'heading'     => __( 'Overlay opacity', 'legocreative' ),
          'value'       => '0.5',
          'param_name'  => 'overlay_opacity',
          'description' => __( 'Value from 0 to 1. for example: 0.5.', 'legocreative' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'On click action', 'js_composer' ),
          'param_name' => 'onclick',
          'value' => array(
            __( 'Open a link', 'legocreative' ) => 'vc_link',
            __( 'Open prettyPhoto', 'legocreative' ) => 'link_image',
          ),
          'description' => __( 'Select action for click action.', 'legocreative' ),
        ),
        array(
          'type' => 'vc_link',
          'heading' => __( 'URL (Link)', 'legocreative' ),
          'param_name' => 'link',
          'description' => __( 'Add link to tilt hover.', 'legocreative' ),
          'dependency' => array(
            'element' => 'onclick',
            'value' => array( 'vc_link' ),
          ),
        ),
        array(
          'type'        => 'textfield',
          'heading'     => __( 'Extra class name', 'legocreative' ),
          'param_name'  => 'el_class',
          'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'legocreative' ),
        ),

        array(
          'type' => 'css_editor',
          'heading' => __( 'CSS box', 'legocreative' ),
          'param_name' => 'css',
          'group' => __( 'Design Options', 'legocreative' ),
        ),
      ),
    );
  }
}

if ( class_exists( 'WPBakeryShortCode' ) ) {
  class WPBakeryShortCode_Marvelous_Tilt extends WPBakeryShortCode {}
}
