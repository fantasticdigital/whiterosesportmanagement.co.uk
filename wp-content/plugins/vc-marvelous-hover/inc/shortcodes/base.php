<?php
if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly.
}

/**
 * Marvelous Shortcode Base class.
 *
 * @since 1.0
 * @package Marvelous Hover Effects
 */

abstract class Marvelous_Shortcode_Base {
  /**
   * Images size
   * @since 1.3
   */
  public $image_size = 'marvelous_hover_size';
  public $image_size_width = 640;

  /**
   * Parent plugin class
   *
   * @var VCMarvelousHover
   * @since  1.0
   */
  protected $plugin = null;

  /**
   * Shortcode shortcode
   *
   * @var String
   * @since  1.0
   */
  protected $shortcode = null;

  /**
   * Shortcode shortcode prefix
   *
   * @var String
   * @since  1.0
   */
  protected $prefix = '';

  /**
   * Constructor
   *
   * @since  1.0
   * @param  VCMarvelousHover $plugin Main plugin object.
   * @return void
   */
  public function __construct( $plugin ) {
    $this->plugin = $plugin;

    if ( marvelous_has_visual_commposer() ) {
      $this->integrateWithVisualComposer();
    }

    $this->hooks();

    // Register CSS and JS
    add_action( 'template_redirect', array( $this, 'registerScripts' ) );

    if ( $this->shortcode && ! shortcode_exists( $this->shortcode ) ) {
      add_shortcode( $this->shortcode, array( $this, 'renderShortcode' ) );
    }
  }

  public function integrateWithVisualComposer() {
    add_action( 'vc_before_init', array( $this, 'maps' ) );
    vc_add_shortcode_param( 'marvelous_class', array( $this, 'classFieldSetting' ) );
    vc_add_shortcode_param( 'marvelous_id', array( $this, 'idFieldSetting' ) );
    vc_add_shortcode_param( 'marvelous_tutorial', array( $this, 'tutorialSetting' ) );
  }

  public function tutorialSetting( $settings, $value ) {
    return
      '<div class="marvelous-tutorial-block"><h4 class="wpb_vc_param_value "><a href="'
      . $settings['value'] . '" target="_blank"><span class="dashicons dashicons-editor-help"></span></a></h4></div>';
  }

  public function classFieldSetting( $settings, $value ) {
    $value = 'marvelous_custom_class_' . uniqid( rand( 8, 10 ) );

    return
      '<div class="my_param_block"><input name="' .
      esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value wpb-textinput ' .
      esc_attr( $settings['param_name'] ) . ' ' .
      esc_attr( $settings['type'] ) . '_field" type="hidden" value="' . esc_attr( $value ) .
      '" /></div>';
  }

  public function idFieldSetting( $settings, $value ) {
    $value = 'marvelous_id_' . uniqid( rand( 8, 10 ) );

    return '<div class="vc_param-vc-grid-id"><input name="'
           . $settings['param_name']
           . '" class="wpb_vc_param_value wpb-textinput '
           . $settings['param_name'] . ' ' . $settings['type'] . '_field" type="hidden" value="'
           . $value . '" /></div>';
  }

  public function maps() {
    $params = $this->params();
    if ( count( $params ) ) {
      $params['name'] = $this->prefix . $params['name'];
      $params['category'] = __( 'Content', 'legocreative' );
      $params['content_element'] = true;
      $params['show_settings_on_create'] = true;
      $params['class'] = '';

      if ( ! isset( $params['icon'] ) ) {
        $params['icon'] = marvelous_hover_assets( 'img/icon.png' );
      }

      if ( ! isset( $params['controls'] ) ) {
        $params['controls'] = 'full';
      }

      vc_map( $params );
    }
  }

  /**
   * Enqueue Scripts
   *
   * @since  1.0.0
   *
   * @return void
   */
  public function enqueueScripts() {}

  /**
   * Custom hooks for shortcode
   *
   * @since  1.0
   * @return void
   */

  public function hooks() {}

  /**
   * Get custom params for shortcode
   *
   * @since  1.0
   * @return {Array} Visual composer shortcode params
   */
  public function params() {
    return array();
  }

  /**
   * Get current shortcode
   *
   * @since  1.0
   * @return string
   */
  public function getShortcode() {
    return $this->shortcode;
  }


  /**
   * Parse shortcode to custom class, custom css
   *
   * @since  1.0.0
   * @return string
   */
  public function getCustomCSS( $atts ) {
    return '';
  }

  /**
   * Render shortcode
   *
   * @since  1.0
   *
   * @param $atts
   * @param null $content
   * @param null $tag
   *
   * @return string
   */
  public function renderShortcode( $atts, $content = null, $tag = null ) {
    marvelous_hover()->enqueueDefaultScripts();
    $this->enqueueScripts();
    return $this->render( $atts, $content );
  }

  /**
   * Render shortcode
   *
   * @since  1.0
   *
   * @param $atts
   * @param null $content
   * @param null $tag
   *
   * @return string
   */
  public function render( $atts, $content = null, $tag = null ) {
    return $this->output( $atts, $content );
  }

  /**
   * @param $atts
   * @param null $content
   * @param string $base
   *
   * vc_filter: vc_shortcode_output - hook to override output of shortcode
   *
   * @return string
   */
  public function output( $atts, $content = null, $base = '' ) {
    $this->atts = $this->prepareAtts( $atts );
    $this->shortcode_content = $content;
    $output = '';
    $content = empty( $content ) && ! empty( $atts['content'] ) ? $atts['content'] : $content;
    if ( marvelous_has_visual_commposer() && ( vc_is_inline() || vc_is_page_editable() ) && method_exists( $this, 'contentInline' ) ) {
      $output .= $this->contentInline( $this->atts, $content );
    } else {
      $custom_output = VCKIT_SHORTCODE_CUSTOMIZE_PREFIX . $this->shortcode;
      $custom_output_before = VCKIT_SHORTCODE_BEFORE_CUSTOMIZE_PREFIX . $this->shortcode; // before shortcode function hook
      $custom_output_after = VCKIT_SHORTCODE_AFTER_CUSTOMIZE_PREFIX . $this->shortcode; // after shortcode function hook

      // Before shortcode
      if ( function_exists( $custom_output_before ) ) {
        $output .= $custom_output_before( $this->atts, $content );
      } else {
        $output .= $this->beforeShortcode( $this->atts, $content );
      }
      // Shortcode content
      if ( function_exists( $custom_output ) ) {
        $output .= $custom_output( $this->atts, $content );
      } else {
        $output .= $this->content( $this->atts, $content );
      }
      // After shortcode
      if ( function_exists( $custom_output_after ) ) {
        $output .= $custom_output_after( $this->atts, $content );
      } else {
        $output .= $this->afterShortcode( $this->atts, $content );
      }
    }
    // Filter for overriding outputs
    $output = apply_filters( 'vc_shortcode_output', $output, $this, $this->atts );

    return $output;
  }

  /**
   * @param $template
   *
   * @return string
   */
  protected function setTemplate( $template ) {
    return $this->html_template = apply_filters( 'marvelous_shortcode_set_template_' . $this->shortcode, $template );
  }

  /**
   * @return bool
   */
  protected function getTemplate() {
    if ( isset( $this->html_template ) ) {
      return $this->html_template;
    }

    return false;
  }

  /**
   * @param $atts
   * @param null $content
   *
   * @return mixed|void
   */
  protected function content( $atts, $content = null ) {
    return $this->loadTemplate( $atts, $content );
  }

  /**
   * @param $atts
   * @param null $content
   *
   * vc_filter: vc_shortcode_content_filter - hook to edit template content
   * vc_filter: vc_shortcode_content_filter_after - hook after template is loaded to override output
   *
   * @return mixed|void
   */
  protected function loadTemplate( $atts, $content = null ) {
    $output = '';
    if ( ! is_null( $content ) ) {
      $content = apply_filters( 'vc_shortcode_content_filter', $content, $this->shortcode );
    }
    $this->findShortcodeTemplate();
    if ( $this->html_template ) {
      ob_start();
      include( $this->html_template );
      $output = ob_get_contents();
      ob_end_clean();
    } else {
      // XXX: trigger_error is not allowed on VIP Production.
      trigger_error( sprintf( __( 'Template file is missing for `%1$s` shortcode. Make sure you have `%1$s` file in your theme folder.', 'js_composer' ), $this->shortcode, 'wp-content/themes/your_theme/vc_templates/' . $this->shortcode . '.php' ) ); // @codingStandardsIgnoreLine
    }

    return apply_filters( 'vc_shortcode_content_filter_after', $output, $this->shortcode );
  }

  /**
   * @param $atts
   *
   * @return array
   */
  protected function prepareAtts( $atts ) {
    $return = array();
    if ( is_array( $atts ) ) {
      foreach ( $atts as $key => $val ) {
        $return[ $key ] = str_replace( array(
          '`{`',
          '`}`',
          '``',
        ), array( '[', ']', '"' ), $val );
      }
    }

    return $return;
  }

  /**
   * Creates html before shortcode html.
   *
   * @param $atts - shortcode attributes list
   * @param $content - shortcode content
   *
   * @return string - html which will be displayed before shortcode html.
   */
  public function beforeShortcode( $atts, $content ) {
    return '';
  }

  /**
   * Creates html before shortcode html.
   *
   * @param $atts - shortcode attributes list
   * @param $content - shortcode content
   *
   * @return string - html which will be displayed after shortcode html.
   */
  public function afterShortcode( $atts, $content ) {
    return '';
  }

  /**
   * @param $el_class
   *
   * @return string
   */
  public function getExtraClass( $el_class ) {
    $output = '';
    if ( '' !== $el_class ) {
      $output = ' ' . str_replace( '.', '', $el_class );
    }

    return $output;
  }

  /**
   * Get CSS animation
   * @param  $css_animation
   * @see    include/classes/shortcodes/shortcodes.php
   * @since  1.0
   *
   * @param  $css_animation
   * @return string
   */
  public function getCSSAnimation( $css_animation ) {
    $output = '';
    if ( '' !== $css_animation ) {
      wp_enqueue_script( 'waypoints' );

      // AnimateCSS effect
      wp_enqueue_style( 'animate-css' );
      $output = ' marvelous-anim marvelous-anim-' . $css_animation;
    }

    return $output;
  }

  /**
   * Find html template for shortcode output.
   */
  protected function findShortcodeTemplate() {
    // Check template path in shortcode's mapping settings
    if ( ! empty( $this->settings['html_template'] ) && is_file( $this->settings( 'html_template' ) ) ) {
      return $this->setTemplate( $this->settings['html_template'] );
    }

    // Check template in theme directory
    $user_template = marvelous_shortcodes_theme_templates( $this->shortcode . '.php' );
    if ( is_file( $user_template ) ) {
      return $this->setTemplate( $user_template );
    }

    // Check default place
    $default_dir = marvelous_hover()->getDefaultShortcodesTemplatesDir();
    if ( is_file( $default_dir . $this->shortcode . '.php' ) ) {
      return $this->setTemplate( $default_dir . $this->shortcode . '.php' );
    }

    return '';
  }

  /**
   * Get image src from attach_image
   * @param  image int
   *
   * @return string
   */
  protected function getImageSrc( $image, $size = 'full' ) {
    $image_src = '';
    if ( '' != $image ) {
      $image_attr = wp_get_attachment_image_src( $image, $size );
      if ( $image_attr ) {
        $image_src = $image_attr[0];
      }
    }

    if ( ! $image_src ) {
      $image_src = marvelous_hover_assets( 'img/no_image.png' );
    }
    return $image_src;
  }

  /**
   * Get image src from attach_image
   * @param  image int
   *
   * @return string
   */
  protected function getImageSrcset( $image, $size = 'full' ) {
    $image_src = '';
    if ( '' != $image ) {
      $image_src = wp_get_attachment_image_srcset( $image, $size );
    }

    if ( ! $image_src ) {
      $image_src = marvelous_hover_assets( 'img/no_image.png' );
    }

    return $image_src;
  }

  protected function addIconLibrary( $dependency = array(), $group = null ) {
    //$pixel_icons = vc_pixel_icons();

    $params = array(
      array(
        'type'    => 'dropdown',
        'heading' => __( 'Icon library', 'legocreative' ),
        'value'   => array(
          __( 'Font Awesome', 'legocreative' ) => 'fontawesome',
          __( 'Open Iconic', 'legocreative' ) => 'openiconic',
          __( 'Typicons', 'legocreative' ) => 'typicons',
          __( 'Entypo', 'legocreative' ) => 'entypo',
          __( 'Linecons', 'legocreative' ) => 'linecons',
          //__( 'Pixel', 'legocreative' ) => 'pixelicons',
          __( 'Mono Social', 'legocreative' ) => 'monosocial',
        ),
        'param_name'  => 'icon_type',
        'std'         => 'fontawesome',
        'description' => __( 'Select icon library.', 'legocreative' ),
        'dependency'  => $dependency,
      ),
      array(
        'type'       => 'iconpicker',
        'heading'    => __( 'Icon', 'legocreative' ),
        'param_name' => 'icon_fontawesome',
        'value'      => 'fa fa-image',
        'settings'   => array(
          'emptyIcon' => false,
          // default true, display an "EMPTY" icon?
          // 'iconsPerPage' => 4000,
          // default 100, how many icons per/page to display
        ),
        'dependency' => array(
          'element' => 'icon_type',
          'value'   => 'fontawesome',
        ),
        'description' => __( 'Select icon from library.', 'legocreative' ),
      ),
      array(
        'type'       => 'iconpicker',
        'heading'    => __( 'Icon', 'legocreative' ),
        'param_name' => 'icon_openiconic',
        'settings'   => array(
          'emptyIcon' => false,
          // default true, display an "EMPTY" icon?
          'type' => 'openiconic',
          // 'iconsPerPage' => 4000,
          // default 100, how many icons per/page to display
        ),
        'dependency' => array(
          'element' => 'icon_type',
          'value' => 'openiconic',
        ),
        'description' => __( 'Select icon from library.', 'legocreative' ),
      ),
      array(
        'type'       => 'iconpicker',
        'heading'    => __( 'Icon', 'legocreative' ),
        'param_name' => 'icon_typicons',
        'settings'   => array(
          'emptyIcon' => false,
          // default true, display an "EMPTY" icon?
          'type' => 'typicons',
          // 'iconsPerPage' => 4000,
          // default 100, how many icons per/page to display
        ),
        'dependency' => array(
          'element' => 'icon_type',
          'value' => 'typicons',
        ),
        'description' => __( 'Select icon from library.', 'legocreative' ),
      ),
      array(
        'type'       => 'iconpicker',
        'heading'    => __( 'Icon', 'legocreative' ),
        'param_name' => 'icon_entypo',
        'settings'   => array(
          'emptyIcon' => false,
          // default true, display an "EMPTY" icon?
          'type' => 'entypo',
          // 'iconsPerPage' => 4000,
          // default 100, how many icons per/page to display
        ),
        'dependency' => array(
          'element' => 'icon_type',
          'value' => 'entypo',
        ),
      ),
      array(
        'type'       => 'iconpicker',
        'heading'    => __( 'Icon', 'legocreative' ),
        'param_name' => 'icon_linecons',
        'settings'   => array(
          'emptyIcon' => false,
          // default true, display an "EMPTY" icon?
          'type' => 'linecons',
          // 'iconsPerPage' => 4000,
          // default 100, how many icons per/page to display
        ),
        'dependency' => array(
          'element' => 'icon_type',
          'value' => 'linecons',
        ),
        'description' => __( 'Select icon from library.', 'legocreative' ),
      ),
      //array(
        //'type'       => 'iconpicker',
        //'heading'    => __( 'Icon', 'legocreative' ),
        //'param_name' => 'icon_pixelicons',
        //'settings'   => array(
          //'emptyIcon' => false,
          //// default true, display an "EMPTY" icon?
          //'type' => 'pixelicons',
          //'source' => $pixel_icons,
        //),
        //'dependency' => array(
          //'element' => 'icon_type',
          //'value' => 'pixelicons',
        //),
        //'description' => __( 'Select icon from library.', 'legocreative' ),
      //),
      array(
        'type'       => 'iconpicker',
        'heading'    => __( 'Icon', 'legocreative' ),
        'param_name' => 'icon_monosocial',
        'value'      => 'vc-mono vc-mono-fivehundredpx', // default value to backend editor admin_label
        'settings'   => array(
          'emptyIcon' => false, // default true, display an "EMPTY" icon?
          'type' => 'monosocial',
          // 'iconsPerPage' => 4000, // default 100, how many icons per/page to display
        ),
        'dependency' => array(
          'element' => 'icon_type',
          'value' => 'monosocial',
        ),
        'description' => __( 'Select icon from library.', 'legocreative' ),
      ),
    );

    if ( $group ) {
      foreach ( $params as $key => $param ) {
        $params[ $key ]['group'] = $group;
      }
    }

    return $params;
  }

  public function getIconClass( $atts ) {
    extract( shortcode_atts( array(
      'icon_type'        => 'fontawesome',
      'icon_fontawesome' => '',
      'icon_openiconic'  => '',
      'icon_typicons'    => '',
      'icon_entypo'      => '',
      'icon_linecons'    => '',
      'icon_monosocial'  => 'vc-mono vc-mono-fivehundredpx',
    ), $atts ) );

    $defaultIconClass = 'fa';
    $iconClass = isset( ${'icon_' . $icon_type} ) ? ${'icon_' . $icon_type} : $defaultIconClass;

    return $iconClass;
  }

  /**
   * Only work with visual composer
   */
  public function getGoogleFonts( $google_fonts, $google_fonts_param_name ) {
    if ( class_exists( 'WPBMap' ) ) {
      // You must have a param with name google_fonts
      $google_fonts_field = WPBMap::getParam( $this->shortcode, $google_fonts_param_name );
      $google_fonts_obj = new Vc_Google_Fonts();
      $google_fonts_field_settings = isset( $google_fonts_field['settings'], $google_fonts_field['settings']['fields'] ) ? $google_fonts_field['settings']['fields'] : array();
      $google_fonts_data = strlen( $google_fonts ) > 0 ? $google_fonts_obj->_vc_google_fonts_parse_attributes( $google_fonts_field_settings, $google_fonts ) : '';

      return $google_fonts_data;
    }

    return null;
  }

  /**
   * Only work with visual composer
   */
  public function getGoogleFontsStyle( $google_fonts_data ) {
    if ( class_exists( 'WPBMap' ) ) {
      $styles = array();

      $google_fonts_family = explode( ':', $google_fonts_data['values']['font_family'] );
      $styles[] = 'font-family:' . $google_fonts_family[0];
      $google_fonts_styles = explode( ':', $google_fonts_data['values']['font_style'] );
      $styles[] = 'font-weight:' . $google_fonts_styles[1];
      $styles[] = 'font-style:' . $google_fonts_styles[2];

      return $styles;
    }

    return '';
  }

  public function isEnableLazyload() {
    return 0 !== marvelous_get_option( 'marvelous_enable_lazyload' );
  }

  function vcIconElementParams() {
    return array(
      'name' => __( 'Icon', 'js_composer' ),
      'base' => 'vc_icon',
      'icon' => 'icon-wpb-vc_icon',
      'category' => __( 'Content', 'js_composer' ),
      'description' => __( 'Eye catching icons from libraries', 'js_composer' ),
      'params' => array(
        array(
          'type' => 'dropdown',
          'heading' => __( 'Icon library', 'js_composer' ),
          'value' => array(
            __( 'Font Awesome', 'js_composer' ) => 'fontawesome',
            __( 'Open Iconic', 'js_composer' ) => 'openiconic',
            __( 'Typicons', 'js_composer' ) => 'typicons',
            __( 'Entypo', 'js_composer' ) => 'entypo',
            __( 'Linecons', 'js_composer' ) => 'linecons',
            __( 'Mono Social', 'js_composer' ) => 'monosocial',
          ),
          'admin_label' => true,
          'param_name' => 'type',
          'description' => __( 'Select icon library.', 'js_composer' ),
        ),
        array(
          'type' => 'iconpicker',
          'heading' => __( 'Icon', 'js_composer' ),
          'param_name' => 'icon_fontawesome',
          'value' => 'fa fa-adjust', // default value to backend editor admin_label
          'settings' => array(
            'emptyIcon' => false,
            // default true, display an "EMPTY" icon?
            'iconsPerPage' => 4000,
            // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
          ),
          'dependency' => array(
            'element' => 'type',
            'value' => 'fontawesome',
          ),
          'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
          'type' => 'iconpicker',
          'heading' => __( 'Icon', 'js_composer' ),
          'param_name' => 'icon_openiconic',
          'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
          'settings' => array(
            'emptyIcon' => false, // default true, display an "EMPTY" icon?
            'type' => 'openiconic',
            'iconsPerPage' => 4000, // default 100, how many icons per/page to display
          ),
          'dependency' => array(
            'element' => 'type',
            'value' => 'openiconic',
          ),
          'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
          'type' => 'iconpicker',
          'heading' => __( 'Icon', 'js_composer' ),
          'param_name' => 'icon_typicons',
          'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
          'settings' => array(
            'emptyIcon' => false, // default true, display an "EMPTY" icon?
            'type' => 'typicons',
            'iconsPerPage' => 4000, // default 100, how many icons per/page to display
          ),
          'dependency' => array(
            'element' => 'type',
            'value' => 'typicons',
          ),
          'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
          'type' => 'iconpicker',
          'heading' => __( 'Icon', 'js_composer' ),
          'param_name' => 'icon_entypo',
          'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
          'settings' => array(
            'emptyIcon' => false, // default true, display an "EMPTY" icon?
            'type' => 'entypo',
            'iconsPerPage' => 4000, // default 100, how many icons per/page to display
          ),
          'dependency' => array(
            'element' => 'type',
            'value' => 'entypo',
          ),
        ),
        array(
          'type' => 'iconpicker',
          'heading' => __( 'Icon', 'js_composer' ),
          'param_name' => 'icon_linecons',
          'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
          'settings' => array(
            'emptyIcon' => false, // default true, display an "EMPTY" icon?
            'type' => 'linecons',
            'iconsPerPage' => 4000, // default 100, how many icons per/page to display
          ),
          'dependency' => array(
            'element' => 'type',
            'value' => 'linecons',
          ),
          'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
          'type' => 'iconpicker',
          'heading' => __( 'Icon', 'js_composer' ),
          'param_name' => 'icon_monosocial',
          'value' => 'vc-mono vc-mono-fivehundredpx', // default value to backend editor admin_label
          'settings' => array(
            'emptyIcon' => false, // default true, display an "EMPTY" icon?
            'type' => 'monosocial',
            'iconsPerPage' => 4000, // default 100, how many icons per/page to display
          ),
          'dependency' => array(
            'element' => 'type',
            'value' => 'monosocial',
          ),
          'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Icon color', 'js_composer' ),
          'param_name' => 'color',
          'value' => array_merge( getVcShared( 'colors' ), array( __( 'Custom color', 'js_composer' ) => 'custom' ) ),
          'description' => __( 'Select icon color.', 'js_composer' ),
          'param_holder_class' => 'vc_colored-dropdown',
        ),
        array(
          'type' => 'colorpicker',
          'heading' => __( 'Custom color', 'js_composer' ),
          'param_name' => 'custom_color',
          'description' => __( 'Select custom icon color.', 'js_composer' ),
          'dependency' => array(
            'element' => 'color',
            'value' => 'custom',
          ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Background shape', 'js_composer' ),
          'param_name' => 'background_style',
          'value' => array(
            __( 'None', 'js_composer' ) => '',
            __( 'Circle', 'js_composer' ) => 'rounded',
            __( 'Square', 'js_composer' ) => 'boxed',
            __( 'Diamond', 'js_composer' ) => 'diamond',
            __( 'Rounded', 'js_composer' ) => 'rounded-less',
            __( 'Outline Circle', 'js_composer' ) => 'rounded-outline',
            __( 'Outline Square', 'js_composer' ) => 'boxed-outline',
            __( 'Outline Rounded', 'js_composer' ) => 'rounded-less-outline',
          ),
          'description' => __( 'Select background shape and style for icon.', 'js_composer' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Background color', 'js_composer' ),
          'param_name' => 'background_color',
          'value' => array_merge( getVcShared( 'colors' ), array( __( 'Custom color', 'js_composer' ) => 'custom' ) ),
          'std' => 'grey',
          'description' => __( 'Select background color for icon.', 'js_composer' ),
          'param_holder_class' => 'vc_colored-dropdown',
          'dependency' => array(
            'element' => 'background_style',
            'not_empty' => true,
          ),
        ),
        array(
          'type' => 'colorpicker',
          'heading' => __( 'Custom background color', 'js_composer' ),
          'param_name' => 'custom_background_color',
          'description' => __( 'Select custom icon background color.', 'js_composer' ),
          'dependency' => array(
            'element' => 'background_color',
            'value' => 'custom',
          ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Size', 'js_composer' ),
          'param_name' => 'size',
          'value' => array_merge( getVcShared( 'sizes' ), array( 'Extra Large' => 'xl' ) ),
          'std' => 'md',
          'description' => __( 'Icon size.', 'js_composer' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Icon alignment', 'js_composer' ),
          'param_name' => 'align',
          'value' => array(
            __( 'Left', 'js_composer' ) => 'left',
            __( 'Right', 'js_composer' ) => 'right',
            __( 'Center', 'js_composer' ) => 'center',
          ),
          'description' => __( 'Select icon alignment.', 'js_composer' ),
        ),
        array(
          'type' => 'vc_link',
          'heading' => __( 'URL (Link)', 'js_composer' ),
          'param_name' => 'link',
          'description' => __( 'Add link to icon.', 'js_composer' ),
        ),
        vc_map_add_css_animation(),
        array(
          'type' => 'textfield',
          'heading' => __( 'Extra class name', 'js_composer' ),
          'param_name' => 'el_class',
          'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
        ),
        array(
          'type' => 'css_editor',
          'heading' => __( 'CSS box', 'js_composer' ),
          'param_name' => 'css',
          'group' => __( 'Design Options', 'js_composer' ),
        ),
      ),
      'js_view' => 'VcIconElementView_Backend',
    );
  }

  public function getVcIcon( $atts ) {

    if ( empty( $atts['i_type'] ) ) {
      $atts['i_type'] = 'fontawesome';
    }
    $data = vc_map_integrate_parse_atts( $this->shortcode, 'vc_icon', $atts, 'i_' );
    if ( $data ) {
      $icon = visual_composer()->getShortCode( 'vc_icon' );
      if ( is_object( $icon ) ) {
        return $icon->render( array_filter( $data ) );
      }
    }

    return '';
  }

  /**
   * @since 1.3
   */
  public function registerScripts() {
    $query_args = apply_filters( 'marvelous_hover_fonts', array(
      'family' => 'Raleway:300,400,800',
      'subset' => 'latin,latin-ext',
    ) );
    wp_register_style( 'marvelous_hover_fonts', add_query_arg( $query_args, '//fonts.googleapis.com/css' ), array(), null );
  }

  /**
   * Load plugin google fonts
   */
  public function loadFonts() {
    wp_enqueue_style( 'marvelous_hover_fonts' );
  }

  public function getEffects( $reverse = false ) {
    $effects = apply_filters( 'marvelous_hover_effect', array(
      __( 'Lily', 'legocreative' )   => 'lily',
      __( 'Sadie', 'legocreative' )  => 'sadie',
      __( 'Honey', 'legocreative' )  => 'honey',
      __( 'Layla', 'legocreative' )  => 'layla',
      __( 'Zoe', 'legocreative' )    => 'zoe',
      __( 'Oscar', 'legocreative' )  => 'oscar',
      __( 'Marley', 'legocreative' ) => 'marley',
      __( 'Ruby', 'legocreative' )   => 'ruby',
      __( 'Roxy', 'legocreative' )   => 'roxy',
      __( 'Bubba', 'legocreative' )  => 'bubba',
      __( 'Romeo', 'legocreative' )  => 'romeo',
      __( 'Dexter', 'legocreative' ) => 'dexter',
      __( 'Sarah', 'legocreative' )  => 'sarah',
      __( 'Chico', 'legocreative' )  => 'chico',
      __( 'Milo', 'legocreative' )   => 'milo',

      // set2
      __( 'Julia', 'legocreative' )   => 'julia',
      __( 'Goliath', 'legocreative' ) => 'goliath',
      __( 'Hera', 'legocreative' )    => 'hera',
      __( 'Winston', 'legocreative' ) => 'winston',
      __( 'Selena', 'legocreative' )  => 'selena',
      __( 'Terry', 'legocreative' )   => 'terry',
      __( 'Phoebe', 'legocreative' )  => 'phoebe',
      __( 'Apollo', 'legocreative' )  => 'apollo',
      __( 'Kira', 'legocreative' )    => 'kira',
      __( 'Steve', 'legocreative' )   => 'steve',
      __( 'Moses', 'legocreative' )   => 'moses',
      __( 'Jazz', 'legocreative' )    => 'jazz',
      __( 'Ming', 'legocreative' )    => 'ming',
      __( 'Lexi', 'legocreative' )    => 'lexi',
      __( 'Duke', 'legocreative' )    => 'duke',
      __( 'Julia', 'legocreative' )   => 'julia',
      __( 'Goliath', 'legocreative' ) => 'goliath',
      __( 'Hera', 'legocreative' )    => 'hera',
      __( 'Winston', 'legocreative' ) => 'winston',
      __( 'Selena', 'legocreative' )  => 'selena',
      __( 'Terry', 'legocreative' )   => 'terry',
      __( 'Phoebe', 'legocreative' )  => 'phoebe',
      __( 'Apollo', 'legocreative' )  => 'apollo',
      __( 'Kira', 'legocreative' )    => 'kira',
      __( 'Steve', 'legocreative' )   => 'steve',
      __( 'Moses', 'legocreative' )   => 'moses',
      __( 'Jazz', 'legocreative' )    => 'jazz',
      __( 'Ming', 'legocreative' )    => 'ming',
      __( 'Lexi', 'legocreative' )    => 'lexi',
      __( 'Duke', 'legocreative' )    => 'duke',

      __( 'Washington', 'legocreative' ) => 'washington',
      __( 'Adams', 'legocreative' ) => 'adams',
      __( 'Madison', 'legocreative' ) => 'madison',
      __( 'Jefferson', 'legocreative' ) => 'jefferson',
      __( 'Jackson', 'legocreative' ) => 'jackson',
      __( 'Van Buren', 'legocreative' ) => 'vanburen',
      __( 'Carter', 'legocreative' ) => 'carter',
      __( 'Cobbles', 'legocreative' ) => 'cobbles',
      __( 'Monroe', 'legocreative' ) => 'monroe',
      __( 'Wilbert', 'legocreative' ) => 'wilbert',

      // Set 4
      __( 'Praia', 'legocreative' ) => 'praia',
      __( 'Brasilia', 'legocreative' ) => 'brasilia',
      __( 'Oslo', 'legocreative' ) => 'oslo',
      __( 'Caracas', 'legocreative' ) => 'caracas',
      __( 'Camberra', 'legocreative' ) => 'camberra',
      __( 'Male', 'legocreative' ) => 'male',
      __( 'Malabo', 'legocreative' ) => 'malabo',
    ));

    if ( $reverse ) {
      $rev = array();

      foreach ( $effects as $key => $value ) {
        $rev[ $value ] = $key;
      }

      return $rev;
    }

    return $effects;
  }

  /**
   * get marvelous custom style for marvelous hover and grid
   * @since 2.0
   */
  public function getMarvelousCustomStyle( $type = 'marvelous', $atts ) {
    $effect =
    $color =
    $hover_color =
    $heading_font_size =
    $description_font_size =
    $image_opacity =
    $hover_image_opacity =
    $overlay_color =
    $hover_overlay_color =
    $custom_class = '';

    extract( $atts );

    $css = '';
    if ( $custom_class ) {
      if ( 'grid' == $type ) {
        $custom_class = '.marvelous-grid.' . $custom_class . ' .effect-hover';
      } else {
        $custom_class = '.effect-hover.' . $custom_class;
      }

      // Fontsize
      if ( $heading_font_size ) {
        if ( preg_match( '/^[0-9]+$/', $heading_font_size ) ) {
          $heading_font_size .= 'px';
        }
        $css .= $custom_class . ' h2 { font-size: ' . $heading_font_size . '; }';
      }
      if ( $description_font_size ) {
        if ( preg_match( '/^[0-9]+$/', $description_font_size ) ) {
          $description_font_size .= 'px';
        }
        $css .= $custom_class . ' p { font-size: ' . $description_font_size . '; }';
      }

      // Color
      if ( $color ) {
        $css .= $custom_class . ' h2, ' . $custom_class . ' p { color: ' . $color . '; }';
      }
      if ( $hover_color ) {
        $css .= $custom_class . ':hover h2, ' . $custom_class . ':hover p { color: ' . $hover_color . ' !important; }';
      }

      // Opacity
      if ( $image_opacity ) {
        $css .= $custom_class . ' img { opacity: ' . $image_opacity . '; }';
      }
      if ( $hover_image_opacity ) {
        $css .= $custom_class . ':hover img { opacity: ' . $hover_image_opacity . '; }';
      }

      // Overlay
      if ( $overlay_color ) {
        $css .= $custom_class . ' { background: ' . $overlay_color . '; }';
      }
      if ( $hover_overlay_color ) {
        if ( ! preg_match( '/adams|jefferson|carter/', $effect ) ) {
          $css .= $custom_class . ':hover { background: ' . $hover_overlay_color . '; }';
        }
      }

      // Set3 customize
      if ( preg_match( '/washington|vanburen|jackson|cobbles|adams|brasilia|oslo|caracas|camberra|malabo/', $effect ) ) {
        $css .= $custom_class . ' figcaption:before { background-color: ' . $hover_overlay_color . '}';
      }
      if ( preg_match( '/male/', $effect ) ) {
        $css .= $custom_class . ' .effect-caption { background-color: ' . $hover_overlay_color . '}';
      }
    }

    return $css;
  }

  // Fix conflict with Ultimate_VC_Addons
  // Fatal error: Call to undefined method EA_Shortcode_Card_Flip::settings() in Ultimate_VC_Addons/modules/Ultimate_Parallax.php on line 28
  public static function settings() {}
}
