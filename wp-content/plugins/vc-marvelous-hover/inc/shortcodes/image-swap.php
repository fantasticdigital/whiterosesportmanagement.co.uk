<?php
if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly.
}


/**
 * Marverlous Hover Shortcode Image Swap class.
 *
 * @since 1.0
 * @package Marvelous Hover Effect
 */

class Marvelous_Shortcode_Image_Swap extends Marvelous_Shortcode_Base {

  protected $shortcode = 'marvelous_image_swap';

  public function params() {
    return array(
      'name'        => __( 'Marvelous Image Swap', 'legocreative' ),
      'description' => __( 'Swap two images when hover', 'legocreative' ),
      'base'        => $this->shortcode,
      'icon' => marvelous_hover_assets( 'img/icon.png' ),
      'class'       => '',
      'params'      => array(
        array(
          'type'       => 'dropdown',
          'heading'    => __( 'Effect' ),
          'param_name' => 'effect',
          'value'      => array(
            __( 'Fade', 'legocreative' )  => 'fade',
            __( 'Slide Downward', 'legocreative' )    => 'slide-downward',
            __( 'Slide Upward', 'legocreative' )    => 'slide-upward',
            __( 'Slide Leftward', 'legocreative' )    => 'slide-leftward',
            __( 'Slide Rightward', 'legocreative' )    => 'slide-rightward',
            ),
          'std'        => 'fade',
          'description' => __( 'Select effect when swapping', 'legocreative' ),
        ),
        array(
          'type'       => 'attach_image',
          'heading'    => __( 'Before image', 'legocreative' ),
          'param_name' => 'beforeimage',
          'edit_field_class' => 'vc_col-sm-6',
          'description' => __( 'Select your image', 'legocreative' ),
        ),
        array(
          'type'       => 'attach_image',
          'heading'    => __( 'After image', 'legocreative' ),
          'param_name' => 'afterimage',
          'edit_field_class' => 'vc_col-sm-6',
          'description' => __( 'Select your image', 'legocreative' ),
        ),
        array(
          'type' => 'vc_link',
          'heading' => __( 'URL (Link)', 'legocreative' ),
          'param_name' => 'link',
          'description' => __( 'Add link.', 'legocreative' ),
        ),
        array(
          'type' => 'checkbox',
          'heading' => __( 'Enable bottom shadow', 'legocreative' ),
          'param_name' => 'shadow',
          'value' => array(
            __( 'Yes', 'legocreative' ) => 'true',
          ),
          'std' => 'true',
          'description' => __( 'Enable bottom shadow when hover', 'legocreative' ),
        ),
        array(
          'type'         => 'textfield',
          'heading'      => __( 'Extra class name', 'legocreative' ),
          'param_name'   => 'el_class',
          'description'  => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.
', 'legocreative' ),
        ),

        array(
          'type' => 'css_editor',
          'heading' => __( 'CSS box', 'legocreative' ),
          'param_name' => 'css',
          'group' => __( 'Design Options', 'legocreative' ),
        ),
      ),
    );
  }

}

if ( class_exists( 'WPBakeryShortCode' ) ) {
  class WPBakeryShortCode_Marvelous_Image_Swap extends WPBakeryShortCode {}
}

