<?php
if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly.
}

/**
 * Marvelous Shortcode Item class.
 *
 * @since 1.0
 * @package Marvelous Hover Effects
 */

class Marvelous_Shortcode_Item extends Marvelous_Shortcode_Base {

  protected $shortcode = 'marvelous_hover';

  /**
   * Override parent constructor
   *
   * @since  1.3
   * @param  VCMarvelousHover $plugin Main plugin object.
   * @return void
   */
  public function __construct( $plugin ) {
    parent::__construct( $plugin );

    if ( 0 !== marvelous_get_option( 'marvelous_enable_image_size' ) ) {
      $this->registerImageSize();
    }
  }

  /**
   * Register image sizes
   *
   * @since 1.3
   * @return void
   */
  public function registerImageSize() {
    add_image_size( $this->image_size . '_43', $this->image_size_width, $this->image_size_width * 3 / 4, true );
    add_image_size( $this->image_size . '_11', $this->image_size_width, $this->image_size_width, true );
    add_image_size( $this->image_size . '_32', $this->image_size_width, $this->image_size_width * 2 / 3, true );
    add_image_size( $this->image_size . '_21', $this->image_size_width, $this->image_size_width / 2, true );
    add_image_size( $this->image_size . '_34', $this->image_size_width, $this->image_size_width * 4 / 3, true );
  }

  public function params() {
    return array(
      'name' => __( 'Marvelous Hover', 'legocreative' ),
      'description' => __( 'Creative and subtle hover effects.', 'legocreative' ),
      'base' => $this->shortcode,
      'icon' => marvelous_hover_assets( 'img/icon.png' ),
      'class' => '',
      'params' => array(
        array(
          'type' => 'dropdown',
          'heading' => __( 'Square ratio', 'legocreative' ),
          'param_name' => 'ratio',
          'value' => array(
            __( 'Original', 'legocreative' )   => 'original',
            __( '4:3', 'legocreative' )        => '43',
            __( '1:1 Square', 'legocreative' ) => '11',
            __( '16:9', 'legocreative' )       => '169',
            __( '3:2', 'legocreative' )        => '32',
            __( '2:1', 'legocreative' )        => '21',
            __( '3:4', 'legocreative' )        => '34',
          ),
          //'std' => '43',
          'description' => __( 'Select image size.', 'legocreative' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'Effect', 'legocreative' ),
          'param_name' => 'effect',
          'value' => $this->getEffects(),
          'std' => 'lily',
          'description' => __( 'Select hover effect you want to display.', 'legocreative' ),
        ),
        array(
          'type'        => 'attach_image',
          'heading'     => 'Image',
          'param_name'  => 'image',
          'description' => __( 'Select image from media library for effect, Default image size should be 960x720', 'legocreative' ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Heading thin text', 'legocreative' ),
          'param_name' => 'heading_thin',
          'description' => __( 'Choose heading thin text', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-6',
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Heading bold text', 'legocreative' ),
          'param_name' => 'heading_bold',
          'description' => __( 'Choose heading bold text', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-6',
        ),
        array(
          'type' => 'textarea',
          'heading' => __( 'Description text', 'legocreative' ),
          'param_name' => 'description',
          'value' => '',
          'description' => __( "Description to uppear, example: Roxy was my best friend. She'd cross any border for me.", 'legocreative' ),
        ),
        array(
          'type' => 'checkbox',
          'heading' => __( 'Use theme default font family?', 'legocreative' ),
          'param_name' => 'disable_font',
          'value' => array( __( 'Yes', 'legocreative' ) => 'yes' ),
          'description' => __( 'Use font family from the theme.', 'legocreative' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => __( 'On click action (enable link)', 'js_composer' ),
          'param_name' => 'enable_link',
          'value' => array(
            __( 'None', 'legocreative' ) => '',
            __( 'Open a link', 'legocreative' ) => 'yes',
            __( 'Open prettyPhoto', 'legocreative' ) => 'link_image',
          ),
          'description' => __( 'Select action for click action.', 'legocreative' ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Url', 'legocreative' ),
          'param_name' => 'url',
          'value' => '',
          'description' => __( 'Add link to image', 'legocreative' ),
          'dependency' => array(
            'element' => 'enable_link',
            'value' => array( 'yes' ),
          ),
        ),
        array(
          'type'        => 'checkbox',
          'heading'     => __( 'Open link in new tab', 'legocreative' ),
          'param_name'  => 'link_new_tab',
          'value'       => array( __( 'Yes', 'legocreative' ) => 'yes' ),
          'description' => __( 'Enable this option to open link in new tab', 'legocreative' ),
          'dependency'  => array(
            'element' => 'enable_link',
            'value'   => array( 'yes' ),
          ),
        ),
        array(
          'type' => 'animation_style',
          'heading' => __( 'Css animation', 'legocreative' ),
          'param_name' => 'css_animation',
          'settings' => array(
            'type' => array(
              'in',
              'other',
            ),
          ),
          'description' => __( 'Select "animation in" for uppear item.', 'legocreative' ),
        ),
        array(
          'type'       => 'textfield',
          'heading'    => __( 'Extra class name', 'legocreative' ),
          'param_name' => 'el_class',
          'description'       => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.
', 'legocreative' ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Image opacity', 'legocreative' ),
          'param_name' => 'image_opacity',
          'group'      => __( 'Customize', 'legocreative' ),
          'description' => __( 'Marvelous hover set opacity on Image to create the overlay. Default: 0.8 and depends on hover effect', 'legocreative' ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Hover image opacity', 'legocreative' ),
          'param_name' => 'hover_image_opacity',
          'group'      => __( 'Customize', 'legocreative' ),
          'description' => __( 'Choose hover image opacity. Default depends on hover effect', 'legocreative' ),
        ),
        array(
          'type'       => 'colorpicker',
          'heading'    => __( 'Text Color', 'legocreative' ),
          'param_name' => 'color',
          'group'      => __( 'Customize', 'legocreative' ),
          'description' => __( 'Select color for heading and paragraph.', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-6',
        ),
        array(
          'type'       => 'colorpicker',
          'heading'    => __( 'Hover text color', 'legocreative' ),
          'param_name' => 'hover_color',
          'group'      => __( 'Customize', 'legocreative' ),
          'description' => __( 'Select color for when hover in.', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-6',
        ),
        array(
          'type'       => 'colorpicker',
          'heading'    => __( 'Overlay color', 'legocreative' ),
          'param_name' => 'overlay_color',
          'group'      => __( 'Customize', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-6',
        ),
        array(
          'type'       => 'colorpicker',
          'heading'    => __( 'Hover overlay color', 'legocreative' ),
          'param_name' => 'hover_overlay_color',
          'group'      => __( 'Customize', 'legocreative' ),
          'edit_field_class' => 'vc_col-sm-6',
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Heading font size', 'legocreative' ),
          'group'      => __( 'Customize', 'legocreative' ),
          'param_name' => 'heading_font_size',
          'description' => __( 'Choose heading font size', 'legocreative' ),
        ),
        array(
          'type' => 'textfield',
          'heading' => __( 'Description font size', 'legocreative' ),
          'group'      => __( 'Customize', 'legocreative' ),
          'param_name' => 'description_font_size',
          'description' => __( 'Choose description font size', 'legocreative' ),
        ),
        array(
          'type'       => 'marvelous_class',
          'heading'    => '',
          'param_name' => 'custom_class',
          'group'      => __( 'Customize', 'legocreative' ),
          'value'      => '',
        ),

        array(
          'type' => 'css_editor',
          'heading' => __( 'CSS box', 'legocreative' ),
          'param_name' => 'css',
          'group' => __( 'Design Options', 'legocreative' ),
        ),
      ),
    );
  }

  public function getCustomCSS( $atts ) {
    return $this->getMarvelousCustomStyle( 'marvelous', $atts );
  }
}

if ( class_exists( 'WPBakeryShortCode' ) ) {
  class WPBakeryShortCode_Marvelous_Hover extends WPBakeryShortCode {}
}
