<?php

/**
 * Marvelous editor shortcodes template
 *
 * @since 2.0
 *
 */

?>
<div id="marvelous_shortcode_form_wrapper">
  <form id="marvelous_shortcode_form" name="marvelous_shortcode_form" method="post" action="" onsubmit="return false">
  <?php
    foreach ( $params as $settings ) {
      if ( 'css_editor' == $settings['type'] ) {
        continue;
      }

      $value = '';
      if ( 'dropdown' == $settings['type'] || 'checkbox' == $setting['type'] ) {
        $value = $settings['std'];
      }
      if ( 'textfield' == $settings['type']
        || 'textarea' == $settings['type']
        || 'colorpicker' == $settings['type'] ) {
        $value = $settings['value'];
      }

      $type_template =
        MARVELOUS_PLUGIN_DIR . '/inc/editor-types/' . $settings['type'] . '.php';
  ?>
    <div class="marvelous-input">
      <div class="marvelous-panel-left">
        <label><?php echo esc_html( $settings['heading'] ) ?></label>
      </div>
      <div class="marvelous-panel-right">
        <div class="marvelous-param">
          <?php
          if ( file_exists( $type_template ) ) {
            include $type_template;
          }
          ?>
        </div>
        <div class="marvelous-description">
          <?php echo $settings['description'] // @codingStandardsIgnoreLine ?>
        </div>
      </div>
    </div>
  <?php } ?>

    <div class="input">
      <button type="submit" class="button-primary marvelous-form-submit">Insert</button>
    </div>
  </form>
</div>

