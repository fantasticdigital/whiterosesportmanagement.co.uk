<?php
/**
 * Marvelous Addons Options
 *
 * @since 0.1
 * @package Marvelous Hover Effects
 */

/**
 * Marvelous Options class.
 *
 * @since 0.1
 */
class Marvelous_Options {
  /**
   * Parent plugin class
   *
   * @var    VCMarvelousHover
   * @since  0.1
   */
  protected $plugin = null;

  /**
   * Option key, and option page slug
   *
   * @var    string
   * @since  0.1
   */
  public $key = 'marvelous_options';

  /**
   * Options page metabox id
   *
   * @var    string
   * @since  0.1
   */
  protected $metabox_id = 'marvelous_options_metabox';

  /**
   * Options Page title
   *
   * @var    string
   * @since  0.1
   */
  protected $title = '';

  /**
   * Options Page hook
   * @var string
   */
  protected $options_page = '';

  /**
   * Constructor
   *
   * @since  0.1
   * @param  VCMarvelousHover $plugin Main plugin object.
   * @return void
   */
  public function __construct( $plugin ) {
    $this->plugin = $plugin;
    $this->hooks();

    $this->title = __( 'Marvelous Hover', 'legocreative' );
  }

  /**
   * Initiate our hooks
   *
   * @since  0.1
   * @return void
   */
  public function hooks() {
    add_action( 'admin_init', array( $this, 'admin_init' ) );
    add_action( 'admin_menu', array( $this, 'add_options_page' ) );
    add_action( 'cmb2_admin_init', array( $this, 'add_options_page_metabox' ) );
  }

  /**
   * Register our setting to WP
   *
   * @since  0.1
   * @return void
   */
  public function admin_init() {
    register_setting( $this->key, $this->key );
  }

  /**
   * Add menu options page
   *
   * @since  0.1
   * @return void
   */
  public function add_options_page() {
    global $submenu;

    if ( isset( $submenu['vc-general'] ) ) {
      $this->options_page = add_submenu_page(
        'vc-general',
        $this->title,
        $this->title,
        'manage_options',
        $this->key,
        array( $this, 'admin_page_display' )
      );
    } else {
      $this->options_page = add_menu_page(
        $this->title,
        $this->title,
        'manage_options',
        $this->key,
        array( $this, 'admin_page_display' )
      );
    }

    // Include CMB CSS in the head to avoid FOUC.
    add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
  }

  /**
   * Admin page markup. Mostly handled by CMB2
   *
   * @since  0.1
   * @return void
   */
  public function admin_page_display() {
    ?>
    <div class="wrap cmb2-options-page <?php echo esc_attr( $this->key ); ?>">
      <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
      <?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
    </div>
    <?php
  }

  /**
   * Add custom fields to the options page.
   *
   * @since  0.1
   * @return void
   */
  public function add_options_page_metabox() {
    $types = marvelous_hover()->__get( 'marvelous_grid' )->getSupportedPostTypes();
    $posttypes = array();
    foreach ( $types as $type ) {
      $posttypes[ $type[0] ] = $type[1];
    }

    $cmb = new_cmb2_box( array(
      'id'         => $this->metabox_id,
      'hookup'     => false,
      'cmb_styles' => false,
      'show_on'    => array(
        // These are important, don't remove.
        'key'   => 'options-page',
        'value' => array( $this->key ),
      ),
    ) );

    $cmb->add_field( array(
      'name'    => __( 'Marvelous grid apply for?', 'legocreative' ),
      'id'      => 'marvelous_grid_post_types',
      'type'    => 'multicheck',
      'desc'    => __( 'Metabox for custom marvelous grid will appear on the post types you selected.', 'legocreative' ),
      'options' => $posttypes,
      'select_all_button' => false,
    ) );

    $cmb->add_field( array(
      'name'    => __( 'Enable lazyload image', 'legocreative' ),
      'id'      => 'marvelous_enable_lazyload',
      'default' => 1,
      'type'    => 'switch',
      'desc'    => __( 'Enable this option, lazyload image will enabled for marvelous hover, marvelous grid', 'legocreative' ),
    ) );

    $cmb->add_field( array(
      'name'    => __( 'Enable custom image sizes', 'legocreative' ),
      'id'      => 'marvelous_enable_image_size',
      'default' => 1,
      'type'    => 'switch',
      'desc'    => __( 'Enable this option, custom image sizes will generated whenever you upload image, <a target="_blank" href="https://developer.wordpress.org/reference/functions/add_image_size/">see more</a>', 'legocreative' ),
    ) );
  }
}
