<?php
if ( ! defined( 'ABSPATH' ) ) {
  die( '-1' );
}

if ( ! function_exists( 'marvelous_get_video_tutorial' ) ) {
  function marvelous_get_video_tutorial( $shortcode ) {
    return 'https://www.youtube.com/watch?v=byGeJUINP7w';
  }
}

if ( ! function_exists( 'marvelous_shortcodes_template' ) ) {
  /**
   * Get shortcode template path in plugin.
   *
   * @param string $name - path name
   * @param string $file
   *
   * @since  1.0
   * @return string
   */
  function marvelous_shortcodes_template( $file = '' ) {
    return marvelous_hover()->path( 'shortcodes', $file );
  }
}

if ( ! function_exists( 'marvelous_shortcodes_theme_templates' ) ) {
  /**
   * Get custom theme template path
   * @since 1.0
   *
   * @param $template - filename for template
   *
   * @return string
   */
  function marvelous_shortcodes_theme_templates( $template ) {
    return marvelous_hover()->getShortcodesTemplateDir( $template );
  }
}

if ( ! function_exists( 'marvelous_hover_set_shortcodes_templates_dir' ) ) {
  /**
   * Sets directory where Marvelous Hover Effects should look for template files for content elements.
   * @since 1.0
   *
   * @param string - full directory path to new template directory with trailing slash
   */
  function marvelous_hover_set_shortcodes_templates_dir( $dir ) {
    marvelous_hover()->setCustomUserShortcodesTemplateDir( $dir );
  }
}

if ( ! function_exists( 'marvelous_shortcode_custom_css_class' ) ) {
  /**
   * @param $param_value
   * @param string $prefix
   *
   * @since 2.0
   * @return string
   */
  function marvelous_shortcode_custom_css_class( $param_value, $prefix = '' ) {
    $css_class = preg_match( '/\s*\.([^\{]+)\s*\{\s*([^\}]+)\s*\}\s*/', $param_value ) ? $prefix . preg_replace( '/\s*\.([^\{]+)\s*\{\s*([^\}]+)\s*\}\s*/', '$1', $param_value ) : '';

    return $css_class;
  }
}
