<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'legocreative_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category marvelous-hover
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( marvelous_hover()->plugin_path() . '/includes/cmb2/init.php' ) ) {
  require_once marvelous_hover()->plugin_path() . '/includes/cmb2/init.php';
} elseif ( file_exists( marvelous_hover()->plugin_path() . '/includes/CMB2/init.php' ) ) {
  require_once marvelous_hover()->plugin_path() . '/includes/CMB2/init.php';
}

/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
function marvelous_hover_register_metabox() {
  $prefix = 'marvelous_grid_';

  /**
   * Sample metabox to demonstrate each field type included
   */
  $posttypes = marvelous_get_option( 'marvelous_grid_post_types' );

  if ( ! isset( $posttypes ) ) {
    return;
  }

  $marvelous_cmb = new_cmb2_box( array(
    'id'            => $prefix . 'metabox',
    'title'         => esc_html__( 'Marvelous Grid Custom Settings', 'legocreative' ),
    'object_types'  => $posttypes, // Post type
    'context'    => 'normal',
    'priority'   => 'low',
    'show_names' => true, // Show field names on the left
    // 'cmb_styles' => false, // false to disable the CMB stylesheet
    'closed'     => true, // true to keep the metabox closed by default
    // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
    // 'classes_cb' => 'legocreative_add_some_classes', // Add classes through a callback.
  ) );

  $marvelous_cmb->add_field( array(
    'name'             => esc_html__( 'Effect', 'legocreative' ),
    'desc'             => esc_html__( 'Select hover effect you want to display for this post.', 'legocreative' ),
    'id'               => $prefix . 'effect',
    'type'             => 'select',
    'show_option_none' => true,
    'options'          => marvelous_hover()->__get( 'marvelous_grid' )->getEffects( true ),
  ) );

  $marvelous_cmb->add_field( array(
    'name' => esc_html__( 'Image instead of featured image', 'legocreative' ),
    'desc' => esc_html__( 'Upload an image or enter a URL. This image will display instead of featured image', 'legocreative' ),
    'id'   => $prefix . 'image',
    'type' => 'file',
    'options' => array(
      'url' => false,
    ),
    'text'    => array(
      'add_upload_file_text' => 'Add Image',
    ),
  ) );

  $marvelous_cmb->add_field( array(
    'name'       => esc_html__( 'Heading thin text', 'legocreative' ),
    'desc'       => esc_html__( 'Choose heading thin text', 'legocreative' ),
    'id'         => $prefix . 'heading_thin',
    'type'       => 'text_small',
  ) );

  $marvelous_cmb->add_field( array(
    'name' => esc_html__( 'Heading bold text', 'legocreative' ),
    'desc' => esc_html__( 'Choose heading bold text', 'legocreative' ),
    'id'   => $prefix . 'heading_bold',
    'type' => 'text_small',
  ) );

  $marvelous_cmb->add_field( array(
    'name' => esc_html__( 'Description text', 'legocreative' ),
    'desc' => esc_html__( 'Description text', 'legocreative' ),
    'id'   => $prefix . 'description',
    'type' => 'text',
  ) );
}

