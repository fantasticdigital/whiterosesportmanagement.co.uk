<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

require_once dirname( __FILE__ ) . '/../cmb2/init.php';
require_once dirname( __FILE__ ) . '/types/CMB2_Type_Switch.php';
