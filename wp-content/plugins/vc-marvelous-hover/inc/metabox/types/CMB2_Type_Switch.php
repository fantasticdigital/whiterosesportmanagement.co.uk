<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly.

/**
 * CMB2_Type_Switch
 *
 * @since 1.0
 * @package Marvelous Hover Effects
 */

require_once dirname( __FILE__ ) . '/../../cmb2/init.php';

if ( ! class_exists( 'CMB2_Type_Switch', false ) ) {
  class CMB2_Type_Switch {

    /**
     * Constructor
     */
    public function __construct( ) {
    }

    public function hooks() {
      add_action( 'cmb2_render_switch', array( $this, 'render' ), 10, 5 );
      add_filter( 'cmb2_sanitize_switch', array( $this, 'cmb2_sanitize_switch_callback' ), 10, 2 );
    }

    public function render( $field, $escaped_value, $object_id, $object_type, $field_type_object ) {
      $params =  array(
        'type'  => 'checkbox',
        'class' => 'themelego-toggle-checkbox',
        'desc'  => '',
        'name'  => $field->args['id'],
        'value' => 1
      );
      if ( $escaped_value ) {
        $params['checked'] = '';
      }
      echo '<div class="themelego-toggle">'
        . $field_type_object->input( $params )
        . '<label class="themelego-toggle-label" for="' . $field->args['id'] . '"></label>'
        . '</div>'
        . '<p class="cmb2-metabox-description">' . $field->args['desc'] . '</p>';
    }

    public function cmb2_sanitize_switch_callback( $override_value, $value ) {
      return $value ? 1 : 0;
    }
  }

  $cmb2_type_switch = new CMB2_Type_Switch();
  $cmb2_type_switch->hooks();
}
