<?php
if ( ! defined( 'ABSPATH' ) ) {
  die( '-1' );
}
/**
 * Shortcode attributes
 *
 * @package Marvelous Hover Effects
 *
 */

$ratio         = '';
$effect        = '';
$image         = '';
$heading_thin  = '';
$heading_bold  = '';
$description   = '';
$disable_font  = '';
$enable_link   = '';
$link_new_tab  = '';
$url           = '';
$css_animation = '';
$el_class      = '';
$custom_class  = '';
$css           = '';

$atts   = marvelous_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts ); // @codingStandardsIgnoreLine

$fonts_class = '';
if ( 'yes' != $disable_font ) {
  $this->loadFonts();
  $fonts_class = 'effect-fonts';
}

$css_classes = array(
  'marvelous-hover',
  'effect-hover',
  'effect-' . $effect,
  'ratio' . $ratio,
  $fonts_class,
  $this->getCSSAnimation( $css_animation ),
  $custom_class,
  marvelous_shortcode_custom_css_class( $css ),
  $el_class,
);

$thumbnail_size = $this->image_size . '_' . $ratio;
if ( 'original' == $ratio ) {
  $thumbnail_size = 'full';
}

if ( $this->isEnableLazyload() ) {
  $css_classes[] = 'marvelous-lazyload';
  $data_image = 'data-src="' . $this->getImageSrc( $image, $thumbnail_size ) . '"';
} else {
  $data_image = 'src="' . $this->getImageSrc( $image, $thumbnail_size ) . '"';
}

$link_attr = '';
$href = 'javascript:;';

$link_classes = array(
  'marvelous-link',
  'noHover', // Conflict with enfold
);

if ( 'yes' == $enable_link ) {
  if ( 'yes' == $link_new_tab ) {
    $link_attr = ' target="_blank"';
  }

  $href = esc_url( $url );
} else if ( 'link_image' === $enable_link ) {
  wp_enqueue_script( 'prettyphoto' );
  wp_enqueue_style( 'prettyphoto' );

  $href = $this->getImageSrc( $image );
  $link_attr .= ' data-rel="prettyPhoto[pp_gal]"';
  $link_classes[] = 'prettyphoto';
}

$image_alt = get_post_meta( $image, '_wp_attachment_image_alt', true );

?>
<a
  href="<?php echo $href //@codingStandardsIgnoreLine  ?>"
  class="<?php echo esc_attr( implode( ' ', $link_classes ) ) ?>"
  <?php echo $link_attr //@codingStandardsIgnoreLine  ?>>
  <figure class="<?php echo esc_attr( implode( ' ', $css_classes ) ) ?>">
    <?php if ( 'male' == $effect ) { ?> <div class="effect-media"> <?php } ?>
    <img <?php echo $data_image //@codingStandardsIgnoreLine ?> alt="<?php echo esc_attr( $image_alt ) ?>"/>
    <?php if ( 'male' == $effect ) { ?> </div> <?php } ?>

    <figcaption>
      <div class="effect-caption">
        <div class="effect-heading">
          <h2><?php echo esc_html( $heading_thin ) ?> <span><?php echo esc_html( $heading_bold ) ?></span></h2>
        </div>

        <div class="effect-description">
          <?php if ( $description  ) { ?>
          <p class="description"><?php echo $description // @codingStandardsIgnoreLine ?></p>
          <?php } else if ( ! preg_match( '/washington|vanburen|jackson|cobbles|madison|adams|jefferson|carter/', $effect ) ) { ?>
          <p class="description">&nbsp;</p>
          <?php } ?>
        </div>

      </div>
    </figcaption>
  </figure>
</a>
