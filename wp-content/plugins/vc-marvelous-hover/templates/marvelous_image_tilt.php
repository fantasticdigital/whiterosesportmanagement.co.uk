<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}
$ratio = $style = $overflow =
$el_class = $css = '';

$atts   = marvelous_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts ); // @codingStandardsIgnoreLine
$el_class = $this->getExtraClass( $el_class );

$css_classes = array(
  'marvelous-image-tilt',
  'ratio' . $ratio,
  marvelous_shortcode_custom_css_class( $css ),
  $el_class,
);

if ( 'true' == $overflow ) {
  $css_classes[] = 'marvelous-image-tilt--border';
}

$options = '{ "extraImgs" : 2, "opacity" : 0.7, "bgfixed" : true, "movement": { "perspective" : 1000, "translateX" : -10, "translateY" : -10, "translateZ" : 20, "rotateX" : 2, "rotateY" : 2, "rotateZ" : 2 } }';

switch ( $style ) {
  case 'style1':
    $options = '{ "opacity" : 0.3, "extraImgs" : 3, "movement": { "perspective" : 1200, "translateX" : -5, "translateY" : -5, "rotateX" : -5, "rotateY" : -5 } }';
    break;
  case 'style2':
    $options = '{ "movement": { "perspective" : 700, "translateX" : -15, "translateY" : -15, "translateZ" : 10, "rotateX" : 2, "rotateY" : 10 } }';
    break;
  case 'style3':
    $options = '{ "opacity" : 0.6, "extraImgs" : 4, "movement": { "perspective" : 500, "translateX" : 15, "translateY" : 0, "translateZ" : 10, "rotateX" : 3, "rotateY" : 4, "rotateZ" : 1 } }';
    break;
  case 'style4':
    $options = '{ "opacity" : 0.8, "bgfixed" : false, "extraImgs" : 3, "movement": { "perspective" : 1500, "translateX" : 80, "translateY" : 80, "translateZ" : 0, "rotateY" : 20 } }';
    break;
  case 'style5':
    $options = '{ "extraImgs" : 4, "opacity" : 0.5, "bgfixed" : true, "movement": { "perspective" : 500, "translateX" : -15, "translateY" : -15, "translateZ" : 20, "rotateX" : 15, "rotateY" : 15 } }';
    break;
  case 'style6':
    $options = '{ "extraImgs" : 2, "opacity" : 0.7, "bgfixed" : false, "movement": { "perspective" : 1000, "translateX" : 30, "translateY" : 30, "translateZ" : -50, "rotateX" : 0, "rotateY" : 0, "rotateZ" : 10 } }';
    break;
  case 'style7':
    $options = '{ "extraImgs" : 2, "extraImgsScaleGrade": -0.05, "opacity" : 0.7, "bgfixed" : false, "movement": { "perspective" : 1000, "translateX" : 30, "translateY" : 30, "translateZ" : -50, "rotateX" : 0, "rotateY" : 0, "rotateZ" : 10 } }';
    break;
  case 'style8':
    $options = '{ "extraImgs" : 2, "opacity" : 0.7, "bgfixed" : false, "resetOnLeave" : false, "movement": { "perspective" : 1000, "translateX" : 30, "translateY" : 30, "translateZ" : -50, "rotateX" : 0, "rotateY" : 0, "rotateZ" : 10 } }';
    break;
  case 'style9':
    $options = '{ "extraImgs" : false, "bgfixed" : false, "movement": { "perspective" : 1000, "translateX" : 30, "translateY" : 30, "translateZ" : -50, "rotateX" : 0, "rotateY" : 0, "rotateZ" : 10 } }';
    break;
  case 'style10':
    $options = '{ "extraImgs" : 2, "opacity" : 0.7, "bgfixed" : false, "customImgsOpacity" : [0.2, 0.5, 0.03], "movement": { "perspective" : 1000, "translateX" : 30, "translateY" : 30, "translateZ" : -50, "rotateX" : 0, "rotateY" : 0, "rotateZ" : 10 } }';
    break;
  case 'custom';
    $options = json_encode( array(
      'extraImgs'           => $atts['extra_imgs'],
      'extraImgsScaleGrade' => $atts['extra_imgs_scale_grade'],
      'opacity'             => $atts['opacity'],
      'bgfixed'             => 'true' == $atts['bgfixed'],
      'movement'            => array(
        'perspective' => $atts['perspective'],
        'translateX'  => $atts['translate_x'],
        'translateY'  => $atts['translate_y'],
        'translateZ'  => $atts['translate_z'],
        'rotateX'     => $atts['rotate_x'],
        'rotateY'     => $atts['rotate_y'],
        'rotateZ'     => $atts['rotate_z'],
      ),
    ) );
    break;
  default:
    break;
}

?>
<figure class="<?php echo esc_attr( implode( ' ', $css_classes ) ) ?>">
  <img
    class="tilt-effect"
    src="<?php echo $this->getImageSrc( $image ); // @codingStandardsIgnoreLine ?>"
    data-tilt-options='<?php echo $options // @codingStandardsIgnoreLine ?>'
    />
</figure>
