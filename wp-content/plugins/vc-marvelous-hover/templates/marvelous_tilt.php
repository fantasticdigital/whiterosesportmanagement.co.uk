<?php
/**
 * Shortcode attributes
 * @var $url
 * @var $player_scale
 * @var $player_color
 * @var $player_color_hover
 * @var $el_class
 * Shortcode class
 * @var $this Marvelous_Shortcode_Tilt
 */

$style = $image = $title = $sub_title = $transition = $link = $color =
$google_fonts = $use_theme_fonts = $google_fonts_data = $el_class =
$overlay_start = $overlay_end = $overlay_style = $overlay_opacity =
$ratio = $onclick = $css = '';

$atts = marvelous_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$settings = get_option( 'wpb_js_google_fonts_subsets' );
if ( is_array( $settings ) && ! empty( $settings ) ) {
  $subsets = '&subset=' . implode( ',', $settings );
} else {
  $subsets = '';
}

if ( 'yes' != $use_theme_fonts ) {
  $google_fonts_data = $this->getGoogleFonts( $google_fonts, 'google_fonts' );
}

if ( isset( $google_fonts_data['values']['font_family'] ) ) {
  wp_enqueue_style( 'vc_google_fonts_' . vc_build_safe_css_class( $google_fonts_data['values']['font_family'] ), '//fonts.googleapis.com/css?family=' . $google_fonts_data['values']['font_family'] . $subsets );
}

$description_styles = array();

if ( 'yes' != $use_theme_fonts ) {
  $google_styles = $this->getGoogleFontsStyle( $google_fonts_data );
}

if ( empty( $google_styles ) ) {
  $google_styles = array();
}

if ( $title_size ) {
  $google_styles[] = 'font-size: ' . $title_size;
}

if ( $color ) {
  $google_styles[] = 'color: ' . $color;
  $description_styles[] = 'color: ' . $color;
}

if ( $sub_title_size ) {
  $description_styles[] = 'font-size: ' . $sub_title_size;
}

$custom_style = 'style="' . esc_attr( implode( ';', $google_styles ) ) . '"';

$css_classes = array(
  'marvelous-tilter',
  'marvelous-tilter--' . $style,
  'noHover', // Conflict with enfold
  marvelous_shortcode_custom_css_class( $css ),
  $el_class,
);

if ( 'yes' == $transition ) {
  $css_classes[] = 'marvelous-smooth';
}

//Overlay
if ( $overlay_start && $overlay_end ) {
  $overlay_style .= 'background-image: linear-gradient(45deg, '
    . $overlay_start . ', ' . $overlay_end . ');';
}
if ( $overlay_opacity ) {
  $overlay_style .= 'opacity: ' . $overlay_opacity;
}

$image_size = $this->image_size;
if ( 'original' == $ratio ) {
  $image_size = 'full';
}

if ( $this->isEnableLazyload() ) {
  $css_classes[] = 'marvelous-lazyload';
  $data_image = 'data-src="' . $this->getImageSrc( $image, $image_size ) . '"';
} else {
  $data_image = 'src="' . $this->getImageSrc( $image, $image_size ) . '"';
}

$link_attribute = '';
if ( 'vc_link' == $onclick ) {
  if ( ! empty( $link ) ) {
    $link = marvelous_build_link( $link );
    $link_attribute = 'href="' . esc_attr( $link['url'] ) . '"'
      . ( $link['target'] ? ' target="' . esc_attr( $link['target'] ) . '"' : '' )
      . ( $link['rel'] ? ' rel="' . esc_attr( $link['rel'] ) . '"' : '' )
      . ( $link['title'] ? ' title="' . esc_attr( $link['title'] ) . '"' : '' );
  }
} else if ( 'link_image' == $onclick ) {
  wp_enqueue_script( 'prettyphoto' );
  wp_enqueue_style( 'prettyphoto' );

  $link_attribute = 'href="' . $this->getImageSrc( $image )
    . '" data-rel="prettyPhoto[pp_gal]"';

  $css_classes[] = 'prettyphoto';
}
?>

<a data-effect="<?php echo esc_attr( $style ) ?>" <?php echo $link_attribute //@codingStandardsIgnoreLine ?> class="<?php echo esc_attr( implode( ' ', $css_classes ) ) ?>">
  <figure class="marvelous-tilter__figure">
    <img class="marvelous-tilter__image" <?php echo $data_image //@codingStandardsIgnoreLine ?> alt="<?php echo esc_attr( $title ) ?>" />
    <div class="marvelous-tilter__deco marvelous-tilter__deco--shine"><div></div></div>
    <div class="marvelous-tilter__deco marvelous-tilter__deco--overlay" style="<?php echo esc_attr( $overlay_style ) ?>"></div>
    <figcaption class="marvelous-tilter__caption">
      <h3 class="marvelous-tilter__title" <?php echo $custom_style // @codingStandardsIgnoreLine ?>><?php echo esc_html( $title ) ?></h3>
      <p class="marvelous-tilter__description" style="<?php echo esc_attr( implode( ';', $description_styles ) ) ?>"><?php echo esc_html( $sub_title ) ?></p>
    </figcaption>
    <svg class="marvelous-tilter__deco marvelous-tilter__deco--lines" viewBox="0 0 300 415" style="<?php if ( $color ) { echo esc_attr( 'stroke: ' . $color ); } ?>">
      <path d="M20.5,20.5h260v375h-260V20.5z" />
    </svg>
  </figure>
</a>
