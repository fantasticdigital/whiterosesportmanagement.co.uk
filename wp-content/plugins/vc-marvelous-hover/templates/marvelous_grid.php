<?php
if ( ! defined( 'ABSPATH' ) ) {
  die( '-1' );
}
/**
 * Shortcode attributes
 *
 * @package Marvelous Hover Effects
 *
 * @var $atts
 * @var $effect
 * @var $post_type
 *
 * Shortcode class
 * @var $this Marvelous_Shortcode_Grid_Item
 */
$isLazyLoad = $this->isEnableLazyload();

$effect =
$element_width =
$gap =
$grid_id =
$el_class =
$custom_class =
$css =
'';

$link_new_tab        = '';
$ratio               = 'original';
$effect              = 'lily';
$disable_item_effect = '';
$disable_font        = '';
$grid_effect = '';

$atts = marvelous_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_classes = array(
  'marvelous-grid',
  'marvelous-grid-animate',
  $grid_effect,
  $custom_class,
  'marvelous-grid-gutter-' . $gap,
);

$thumbnail_size = $this->image_size . '_' . $ratio;
if ( 'original' == $ratio ) {
  $thumbnail_size = 'full';
}

if ( $isLazyLoad ) {
  $css_classes[] = 'marvelous-lazyload';
}

$wrap_classes = array(
  'marvelous-grid-wrap',
  $el_class,
  marvelous_shortcode_custom_css_class( $css ),
);

$this->addExcludedId( $this->postID() );
if ( 'custom' === $atts['post_type'] && ! empty( $atts['custom_query'] ) ) {
  $query = html_entity_decode( marvelous_value_from_safe( $atts['custom_query'] ), ENT_QUOTES, 'utf-8' );
  $post_data = query_posts( $query );
} else {
  $settings = $this->filterQuerySettings( $this->buildQuery( $atts ) );
  $post_data = query_posts( $settings );
}

$fonts_class = '';
if ( 'yes' != $disable_font ) {
  $this->loadFonts();
  $fonts_class = 'effect-fonts';
}
?>
<?php if ( $post_data ) { ?>
<div class="<?php echo esc_attr( implode( ' ', $wrap_classes ) ); ?>">
  <div class="<?php echo esc_attr( implode( ' ', $css_classes ) ); ?>" id="<?php echo esc_attr( $grid_id ) ?>">
    <?php foreach ( $post_data as $item ) { ?>
    <div class="marvelous-grid-item mg-col-xs-12 <?php echo $element_width < 12 ? 'mg-col-sm-6' : 'mg-col-sm-12' ?> mg-col-md-<?php echo esc_attr( $element_width ) ?>">
      <div class="marvelous-grid-item-wrap">
        <?php
          // Get metadata
          $meta_effect       = get_post_meta( $item->ID, 'marvelous_grid_effect', true );
          $meta_image_id     = get_post_meta( $item->ID, 'marvelous_grid_image_id', true );
          $meta_heading_thin = get_post_meta( $item->ID, 'marvelous_grid_heading_thin', true );
          $meta_heading_bold = get_post_meta( $item->ID, 'marvelous_grid_heading_bold', true );
          $meta_description  = get_post_meta( $item->ID, 'marvelous_grid_description', true );

          // Image src is meta_image or post feature image
          $image = '';
          // meta_image_id sometimes return '0', sometimes return ''
          if ( 'attachment' == $post_type ) {
            $image = $item->ID;
          } else if ( '' != $meta_image_id && '0' != $meta_image_id ) {
            $image = $meta_image_id;
          } else {
            $image = get_post_thumbnail_id( $item->ID );
          }

          // Ratio is grid $ratio
          // Heading thin is item meta heading thin
          // Heading bold is item meta heading bold
          // Description is meta description
          $description = $meta_description;

          $item_classes = array(
            'effect-hover',
            'ratio' . $ratio,
            $fonts_class,
          );

          // Effect is item effect or Grid effect (still grid effect if item effect is null)
          if ( 'yes' == $disable_item_effect || '' == $meta_effect ) {
            $item_effect = 'effect-' . $effect;
          } else {
            $item_effect = 'effect-' . $meta_effect;
          }

          $item_classes[] = $item_effect;

          if ( $isLazyLoad ) {
            $data_image = 'data-src="' . $this->getImageSrc( $image, $thumbnail_size ) . '"';
          } else {
            $data_image = 'src="' . $this->getImageSrc( $image, $thumbnail_size ) . '"';
          }

          $link_attr = '';
          $href = 'javascript:;';

          $link_classes = array(
            'marvelous-link',
            'noHover', // Conflict with enfold
          );

          if ( 'yes' == $enable_link ) {
            if ( 'yes' == $link_new_tab ) {
              $link_attr = ' target="_blank"';
            }

            $href = get_the_permalink( $item->ID );
          } else if ( 'link_image' === $enable_link ) {
            wp_enqueue_script( 'prettyphoto' );
            wp_enqueue_style( 'prettyphoto' );

            $href = $this->getImageSrc( $image );
            $link_attr .= ' data-rel="prettyPhoto[pp_gal]"';
            $link_classes[] = 'prettyphoto';
          }

          $image_alt = get_post_meta( $image, '_wp_attachment_image_alt', true );
        ?>
        <a
          href="<?php echo $href; //@codingStandardsIgnoreLine ?>"
          class="<?php echo esc_attr( implode( ' ', $link_classes ) ) ?>"
          <?php echo $link_attr //@codingStandardsIgnoreLine  ?>>
          <figure class="<?php echo esc_attr( implode( ' ', $item_classes ) ) ?>">

            <?php if ( 'male' == $item_effect ) { ?> <div class="effect-media"> <?php } ?>
              <img <?php echo $data_image //@codingStandardsIgnoreLine ?> alt="<?php echo esc_attr(  $image_alt ) ?>"/>
            <?php if ( 'male' == $item_effect ) { ?> </div> <?php } ?>

            <figcaption>
              <div class="effect-caption">
                <div class="effect-heading">
                  <h2>
                    <?php if ( '' != $meta_heading_thin && '' != $meta_heading_bold ) { ?>
                      <?php echo esc_html( $meta_heading_thin ) ?> <span><?php echo esc_html( $meta_heading_bold ) ?></span>
                    <?php } ?>
                  </h2>
                </div>
                <div class="effect-description">
                  <p class="description">
                    <?php if ( '' != $description ) { ?>
                      <?php echo esc_html( $description ) ?>
                    <?php } else { ?>
                      <?php echo esc_html( $item->post_title ) ?>
                    <?php } ?>
                  </p>
                </div>
              </div>
            </figcaption>
          </figure>
        </a>
      </div>
    </div>
    <?php } ?>
  </div>
</div>
<?php } ?>
