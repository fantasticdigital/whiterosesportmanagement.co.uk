<?php
if ( ! defined( 'ABSPATH' ) ) {
  die( '-1' );
}

$effect = $beforeimage = $afterimage = $el_class = $link = $shadow = $css = '';

$atts   = marvelous_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts ); // @codingStandardsIgnoreLine
$el_class = $this->getExtraClass( $el_class );

$css_classes = array(
  'marvelous-imsw',
  marvelous_shortcode_custom_css_class( $css ),
  $el_class,
);

if ( $this->isEnableLazyload() ) {
  $css_classes[] = 'marvelous-lazyload';
  $data_image = 'data-src="' . $this->getImageSrc( $beforeimage ) . '"';
} else {
  $data_image = 'src="' . $this->getImageSrc( $beforeimage ) . '"';
}


$link_attribute = '';

if ( ! empty( $link ) ) {
  $link = vc_build_link( $link );

  $link_attribute = 'href="' . esc_attr( $link['url'] ) . '"'
    . ( $link['target'] ? ' target="' . esc_attr( $link['target'] ) . '"' : '' )
    . ( $link['rel'] ? ' rel="' . esc_attr( $link['rel'] ) . '"' : '' )
    . ( $link['title'] ? ' title="' . esc_attr( $link['title'] ) . '"' : '' );
}

if ( 'true' == $shadow ) {
  $css_classes[] = 'marvelous-imsw-shadow';
}

?>
<?php if ( ! empty( $link ) ) { ?>
<a class="marvelous-imsw-link" <?php echo $link_attribute ?> />
<?php } ?>

<div class="marvelous-imsw-wrapper">
  <div
    class="<?php echo esc_attr( implode( ' ', $css_classes ) ) ?>"
    data-hover-style="<?php echo esc_attr( $effect ) ?>">
    <img <?php echo $data_image //@codingStandardsIgnoreLine ?> class="imsw-bf-img">
    <div class="imsw-af-img" style="background-image: url(<?php echo esc_url( $this->getImageSrc( $afterimage ) ); ?>)"></div>
  </div>
</div>
<?php if ( ! empty( $link ) ) { ?>
</a>
<?php } ?>
